//
//  Animation.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/14/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class Animation {
    var sprites : [SKTexture] = []
    var delay : NSTimeInterval
    var timer : NSTimeInterval
    
    init(sprites : [SKTexture], delay : NSTimeInterval) {
        self.delay = delay
        self.sprites = sprites
        timer = 0
    }
    
    init (spriteName : String, amountOfSprites : Int, delay : NSTimeInterval) {
        timer = 0
        self.delay = delay
        sprites = loadSprites(spriteName, amount: amountOfSprites)
    }
    
    init (spriteName : String, range : Range<Int>, delay : NSTimeInterval) {
        timer = 0
        self.delay = delay
        sprites = loadSprites(spriteName, range: range)
    }
    
    init (spriteName : String, amountOfSprites : Int, delay : NSTimeInterval, startingFrame : Int) {
        timer = NSTimeInterval(startingFrame) * delay
        self.delay = delay
        sprites = loadSprites(spriteName, amount: amountOfSprites)
    }
    
    init (spriteName : String, amountOfSprites : Int, delay : NSTimeInterval, startingTime : NSTimeInterval) {
        timer = startingTime
        self.delay = delay
        sprites = loadSprites(spriteName, amount: amountOfSprites)
    }
    
    var currentFrame : Int {
        get {
            if timer < timeLength {
                return Int(floor(Double(timer/delay)))
            } else {
                return sprites.count - 1
            }
        }
        
        set (newFrame) {
            timer = NSTimeInterval(newFrame) * delay
        }
    }
    
    var currentTexture : SKTexture {
        get {
            return sprites[currentFrame]
        }
    }
    
    var timeLength : NSTimeInterval {
        get {
            return delay * NSTimeInterval(sprites.count - 1)
        }
    }
    
    var timeBeforeFinish : NSTimeInterval {
        get {
            return timeLength - timer
        }
    }
    
    var SKAnimation : SKAction {
        return SKAction.animateWithTextures(sprites, timePerFrame: delay)
    }
    
    func loadSprites(name : String, amount : Int) -> [SKTexture] {
        var s : [SKTexture] = []
        for i in 0..<amount {
            var texture = SKTexture(textureWithoutAA: "\(name)\(i)")
            s.append(texture)
        }
        return s
    }
    
    func loadSprites(name : String, range : Range<Int>) -> [SKTexture] {
        var s : [SKTexture] = []
        for i in range {
            var texture = SKTexture(textureWithoutAA: "\(name)\(i)")
            s.append(texture)
        }
        return s
    }
    
    func reset() {
        timer = 0
    }
    
    func update(delta : NSTimeInterval) {
        timer += delta
    }
}