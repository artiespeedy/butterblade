//
//  Array2D.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/14/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

struct Array2D<T> {
    var data : [[T]]
    
    var width : Int {
        get {
            return data.count
        }
    }
    
    var height : Int {
        get {
            return data[0].count
        }
    }
    
    var count : Int {
        get {
            return width * height
        }
    }
    
    var center : CGPoint {
        get {
            return CGPoint(x: width / 2, y: height / 2)
        }
    }
    
    subscript(xIndex : Int) -> [T] {
        get {
            return data[xIndex]
        }
        set(newValue) {
            data[xIndex] = newValue
        }
    }
    
    init(width : Int, height : Int, fillerValue : T) {
        data = [[T]](count: width, repeatedValue: [T](count: height, repeatedValue: fillerValue))
    }
    
    mutating func shift(x : Int, distance : Int, fillerValue : T) {
        let buffer = [T](count: abs(distance), repeatedValue: fillerValue)
        var newArray : [T] = []
        if distance > 0 {
            newArray = buffer + self[x]
            let range = newArray.count - abs(distance)..<newArray.count
            newArray.removeRange(range)
        } else {
            newArray = self[x] + buffer
            let range = 0..<abs(distance)
            newArray.removeRange(range)
        }
        for i in 0..<newArray.count {
            data[x][i] = newArray[i]
        }
    }
}