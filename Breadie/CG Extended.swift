//
//  CG Extended.swift
//  Breadie
//
//  Created by Alex Mai on 7/30/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

//CGPoint

//Multiply

func * (lhs : CGPoint, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x * rhs.x, y: lhs.y * rhs.y)
}

func * (lhs : CGPoint, rhs : CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

func *= (inout lhs : CGPoint, rhs : CGFloat) {
    lhs = lhs * rhs
}

func *= (inout lhs : CGPoint, rhs : CGPoint) {
    lhs = lhs * rhs
}

//Divide

func / (lhs : CGPoint, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x / rhs.x, y: lhs.y / rhs.y)
}

func / (lhs : CGPoint, rhs : CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x / rhs, y: lhs.y / rhs)
}

func /= (inout lhs : CGPoint, rhs : CGFloat) {
    lhs = lhs / rhs
}

func /= (inout lhs : CGPoint, rhs : CGPoint) {
    lhs = lhs / rhs
}

//Add

func + (lhs : CGPoint, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

func + (lhs : CGPoint, rhs : CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x + rhs, y: lhs.y + rhs)
}

func += (inout lhs : CGPoint, rhs : CGFloat) {
    lhs = lhs + rhs
}

func += (inout lhs : CGPoint, rhs : CGPoint) {
    lhs = lhs + rhs
}

//Subtract

func - (lhs : CGPoint, rhs : CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

func - (lhs : CGPoint, rhs : CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x - rhs, y: lhs.y - rhs)
}

func -= (inout lhs : CGPoint, rhs : CGFloat) {
    lhs = lhs - rhs
}

func -= (inout lhs : CGPoint, rhs : CGPoint) {
    lhs = lhs - rhs
}

//CGSize

//Multiply

func * (lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width * rhs.width, height: lhs.height * rhs.height)
}

func * (lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
}

func *= (inout lhs : CGSize, rhs : CGFloat) {
    lhs = lhs * rhs
}

func *= (inout lhs : CGSize, rhs : CGSize) {
    lhs = lhs * rhs
}

//Divide

func / (lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width / rhs.width, height: lhs.height / rhs.height)
}

func / (lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width / rhs, height: lhs.height / rhs)
}

func /= (inout lhs : CGSize, rhs : CGFloat) {
    lhs = lhs / rhs
}

func /= (inout lhs : CGSize, rhs : CGSize) {
    lhs = lhs / rhs
}

//Add

func + (lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
}

func + (lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width + rhs, height: lhs.height + rhs)
}

func += (inout lhs : CGSize, rhs : CGFloat) {
    lhs = lhs + rhs
}

func += (inout lhs : CGSize, rhs : CGSize) {
    lhs = lhs + rhs
}

//Subtract

func - (lhs : CGSize, rhs : CGSize) -> CGSize {
    return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
}

func - (lhs : CGSize, rhs : CGFloat) -> CGSize {
    return CGSize(width: lhs.width - rhs, height: lhs.height - rhs)
}

func -= (inout lhs : CGSize, rhs : CGFloat) {
    lhs = lhs - rhs
}

func -= (inout lhs : CGSize, rhs : CGSize) {
    lhs = lhs - rhs
}

//CGFloat

extension CGFloat {
    mutating func cap(cap : CGFloat) {
        if !self.isNegative {
            if self > cap {
                self = cap
            }
        } else {
            if self < cap {
                self = cap
            }
        }
    }
    
    var isNegative : Bool {
        get {
            if self == 0 {
                return false
            } else if (self / abs(self)) == -1 {
                return true
            }
            return false
        }
    }
}

extension CGPoint {
    var length : CGFloat {
    get {
        return CGFloat(sqrt(pow(self.x, 2) + pow(self.y, 2)))
    }
    }
    
    func toCGVector() -> CGVector {
        return CGVectorMake(self.x, self.y)
    }
    
    func reverse() -> CGPoint {
        return CGPoint(x: -self.x, y: -self.y)
    }
    
    var iX : Int {
        get {
            return Int(floor(self.x))
        }
    }
    
    var iY : Int {
        get {
            return Int(floor(self.x))
        }
    }
}

extension CGRect {
    var topRight : CGPoint {
    get {
        return CGPoint(x: self.origin.x + self.width, y: self.origin.y + self.height)
    }
    }
    
    var topLeft : CGPoint {
    get {
        return CGPoint(x: self.origin.x, y: self.origin.y + self.height)
    }
    }
    
    var bottomRight : CGPoint {
    get {
        return CGPoint(x: self.origin.x + self.width, y: self.origin.y)
    }
    }
    
    var bottomLeft : CGPoint {
    get {
        return self.origin
    }
    }
    
    var centerPoint : CGPoint {
    get {
        return self.origin + CGPoint(x: self.width / 2, y: self.height / 2)
    }
    }
}
