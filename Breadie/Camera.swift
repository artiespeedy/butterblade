//
//  Camera.swift
//  ButterBlade
//
//  Created by Eric Mai on 8/12/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class Camera : SKNode {
    var objects : [SKNode]
    var view : CGRect
    var isRunning = false
    var entities : EntityDelegate? {
        didSet {
            addChild(entities!)
        }
    }
    var map : Map? {
        didSet {
            addChild(map!)
        }
    }
    
    override init() {
        objects = [SKSpriteNode()]
        view = CGRectZero
        super.init()
    }
    
    init(view : CGRect) {
        objects = []
        self.view = view
        super.init()
    }
    
    init(objects : [SKNode], entityDelegate : EntityDelegate, map : Map, view : CGRect) {
        entities = entityDelegate
        self.map = map
        self.objects = objects
        self.view = view
        super.init()
        //Add objects to view
        for i in objects {
            addChild(i)
        }
        addChild(map)
        addChild(entities!)
    }
    
    init(entityDelegate : EntityDelegate, map : Map, view : CGRect) {
        entities = entityDelegate
        self.map = map
        self.objects = []
        self.view = view
        super.init()
        //Add objects to view
        addChild(map)
        addChild(entities!)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoder not supported in Camera.swift")
    }
    
    var centerPoint : CGPoint {
        get {
            return CGPoint(x: view.width / 2, y: view.height / 2)
        }
    }
    
    func addObject(object : SKNode) {
        objects.append(object)
        self.addObject(object)
    }
    
    func centerAround(node : SKNode) {
        var nodePositionOnScreen = node.position + self.position
        var nodeDistanceFromCenter = nodePositionOnScreen - self.view.centerPoint
        position -= nodeDistanceFromCenter
    }
    
    func centerOnMain() {
        if entities?.mainEntity != nil {
            var ePos : CGPoint
            if entities!.mainEntity!.xScale < 0 {
                ePos = entities!.mainEntity!.reversedPosition
            } else {
                ePos = entities!.mainEntity!.position
            }
            var nodePositionOnScreen = ePos + self.position
            var nodeDistanceFromCenter = nodePositionOnScreen - self.view.centerPoint
            position -= nodeDistanceFromCenter
        } else {
            println("No main entity")
        }
    }
    
    func setPositionOnMap(point : CGPoint) {
        position -= point
    }
}