//
//  CameraDelegate.swift
//  ButterBlade
//
//  Created by Eric Mai on 8/9/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class CameraDelegate {
    var mainEntity : Entity
    var map : Map
    var focusRect : CGRect
    var cameraPanSpeed : CGFloat
    var parent : SKScene?
    var target : CGPoint?
    var timeSinceLastAction : CGFloat
    
    init(map : Map, entity : Entity, focus : CGRect, speed : CGFloat) {
        self.map = map
        mainEntity = entity
        focusRect = focus
        cameraPanSpeed = speed
        timeSinceLastAction = 0
        
    }
    
    init() {
        map = Map(name: "LevelTest", type: "txt")
        mainEntity = Entity(imageName: "Tile 0")
        focusRect = CGRect()
        cameraPanSpeed = 5
        timeSinceLastAction = 0
    }
    
    func update(delta : CGFloat) {
        
        if target != nil {
            if !focusRect.contains(mainEntity.position) {
                var x = focusRect.centerPoint.x - mainEntity.position.x
                var y = focusRect.centerPoint.y - mainEntity.position.y
                var length = CGPoint(x: x, y: y).length
                var action = SKAction.moveByX(-x, y: -y, duration: NSTimeInterval(length / cameraPanSpeed))
                timeSinceLastAction = length / cameraPanSpeed
                mainEntity.runAction(action)
                map.runAction(action)
            }
        }
        
        if timeSinceLastAction <= 0 {
            timeSinceLastAction = 0
            target = nil
        } else {
            timeSinceLastAction -= delta
        }
        
    }
}