//
//  CollisionTile.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/12/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//
/*
import Foundation
import SpriteKit

class CollisionTile {
    let isSolid : Bool
    let isHazardous : Bool
    let positionInScene : CGPoint
    let tileSize : Int
    let isSlope : Bool
    var rects : [CGRect] = []
    var direction : Directions
    var currentOriginTile = CGPointZero
    
    var tileSizeMultiplier : CGFloat {
        get {
            return CGFloat(tileSize) / CGFloat(DEFAULT_TILE_SIZE)
        }
    }
    
    var rectInScene : CGRect {
        get {
            return CGRect(x: positionInScene.x, y: positionInScene.y, width: CGFloat(tileSize), height: CGFloat(tileSize))
        }
    }
    
    var slopedRectsInScene : [CGRect] {
        get {
            var slopedRects : [CGRect] = []
            
            let multiplier = CGFloat(tileSize / DEFAULT_TILE_SIZE)
            
            for i in rects {
                slopedRects.append(CGRect(x: positionInScene.x + (i.origin.x * multiplier), y: positionInScene.y, width: multiplier, height: i.height * multiplier))
            }
            
            return slopedRects
        }
    }
    
    var description : String {
        get {
            switch ((isSolid, isHazardous, isSlope)) {
            case (true, true, true):
                return "Sloped solid block that is hazardous with location \(positionInScene)."
            case (false, false, false):
                return "Air block with location \(positionInScene)."
            case (true, false, true):
                return "Sloped solid block with location \(positionInScene)."
            case (true, false, false):
                return "Solid block with location \(positionInScene)."
            default:
                return "A boring block."
            }
        }
    }
    
    func printDescription() {
        println(description)
    }
    
    init(position : CGPoint, tileSize : Int, isSolid : Bool, isHazardous : Bool) {
        self.positionInScene = position
        self.tileSize = tileSize
        self.isSolid = isSolid
        self.isHazardous = isHazardous
        self.isSlope = false
        self.direction = Directions.None
    }
    
    init(position : CGPoint, tileSize : Int, isSolid : Bool, isHazardous : Bool, collisionRects : [CGRect], direction : Directions) {
        self.positionInScene = position
        self.tileSize = tileSize
        self.isSolid = isSolid
        self.isHazardous = isHazardous
        self.isSlope = true
        rects = collisionRects
        self.direction = direction
    }
    
    init(fromTile : Tile, andPosition : CGPoint, andTileSize : Int) {
        self.positionInScene = andPosition
        self.tileSize = andTileSize
        self.isSolid = fromTile.isSolid
        self.isHazardous = fromTile.isHazardous
        self.direction = fromTile.direction
        if fromTile.isSloped {
            self.isSlope = true
            rects = fromTile.slopedBoundingBoxes
        } else {
            self.isSlope = false
        }
    }
    
    func highestPointTouching(rect : CGRect) -> CGFloat {
        if self.isSlope {
            var highest : Int = 0
            for i in slopedRectsInScene {
                if rect.intersects(i) && Int(i.height) > highest {
                    highest = Int(i.height)
                }
            }
            return self.rectInScene.origin.y + CGFloat(highest)
        } else {
            return self.rectInScene.topLeft.y
        }
    }
    
    func slopeHeightAt(x : CGFloat) -> Int {
        var tileX = Int(floor(x / CGFloat(tileSize)))
        var pixelX = Int(floor(x) - CGFloat(tileX * tileSize))
        var slopeX = Int(CGFloat(pixelX) / tileSizeMultiplier)
        if isSlope {
            if slopeX < 16 && slopeX >= 0 {
                return Int(rects[slopeX].height)
            }/* else if (slopeX == 16) {
                return Int(rects[slopeX - 1].height)
            }*/ else {
                return 16
            }
        } else if isSolid {
            return 16
        } else {
            return 0
        }
    }
    
    func slopeRectForX(x : CGFloat) -> CGRect {
        var tileX = Int(floor(x / CGFloat(tileSize)))
        var pixelX = Int(floor(x) - CGFloat(tileX * tileSize))
        var slopeX = Int(CGFloat(pixelX) / tileSizeMultiplier)
        if isSlope {
            if slopeX < 16 && slopeX >= 0 {
                return rects[slopeX]
            }
        }
        return CGRectZero
    }
    
    func heightAt(x : CGFloat) -> CGFloat {
        return (CGFloat(slopeHeightAt(x)) * tileSizeMultiplier) + rectInScene.bottomRight.y
    }
}*/