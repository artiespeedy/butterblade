//
//  Directions.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/16/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation

enum Directions : Int {
    case Up = 0
    case Right = 1
    case Down = 2
    case Left = 3
    case Forward = 4
    case Downward = 5
    case None = 6
    
    func toStringRaw() -> String {
        switch(self.toRaw()) {
        case 0:
            return "Up"
        case 1:
            return "Right"
        case 2:
            return "Down"
        case 3:
            return "Left"
        case 4:
            return "Forward"
        case 5:
            return "Downward"
        case 6:
            return "None"
        default:
            return "One"
        }
    }
}