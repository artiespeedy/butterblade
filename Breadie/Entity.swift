//
//  Entity.swift
//  Breadie
//
//  Created by Alex Mai on 7/26/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let GRAVITYPULL = CGPoint(x: 0, y: -1000)

enum EntityType : Int {
    case Main = 0
    case Player = 1
    case Static = 2
    case Aggressive = 3
    case Passive = 4
    case Peaceful = 5
    
    /*func fromInt(num : Int) -> EntityType {
        switch(num) {
        case 0:
            return EntityType.Main
        case 1:
            return EntityType.Player
        case 2:
            return EntityType.Static
        case 3:
            return EntityType.Aggressive
        case 4:
            return EntityType.Passive
        case 5:
            return EntityType.Peaceful
        default:
            return EntityType.Static
        }
    }*/
}

class Entity : SKSpriteNode {
    var type : EntityType
    var hasMass = true
    var friction : CGFloat = 10
    var DEFAULT_SIZE : CGSize
    var walkingSpeed : CGFloat = 60
    var maxSlopeStepUp : CGFloat = 20
    
    var velocity = CGPoint(x: 0, y: 0)
    var desiredPosition : CGPoint
    
    var currentDirection = Directions.None
    var terminalVelocity : CGFloat = 30
    
    var isJumping = false
    var jumpVelocity = CGPoint(x: 0, y: 0)
    var jumpTimer : NSTimeInterval = 0
    var jumpStrength = CGPoint(x: 0, y: 300)
    
    var externalVelocities = CGPoint(x: 0, y: 0)
    
    var originalSize : CGSize
    var originalFeetSize : CGSize
    
    var weapon : Weapon?
    
    var jumping : SKTexture
    var walking : LoopingAnimation
    var idle : SKTexture
    
    init(imageName : String, colliderSize : CGSize, feetHitSize : CGSize, entityType : EntityType) {
        self.originalSize = colliderSize
        self.originalFeetSize = feetHitSize
        var texture = SKTexture(imageNamed: imageName)
        desiredPosition = CGPointZero
        texture.filteringMode = SKTextureFilteringMode.Nearest
        DEFAULT_SIZE = texture.size()
        jumping = SKTexture(textureWithoutAA: "BreadieJump0")
        walking = LoopingAnimation(spriteName: "BreadieWalk", amountOfSprites: 14, delay: 0.08)
        idle = SKTexture(textureWithoutAA: "BreadieIdle0")
        type = entityType
        super.init(texture: texture, color: UIColor.whiteColor(), size: texture.size())
    }
    
    init(imageName : String, weaponName : String, colliderSize : CGSize, feetHitSize : CGSize, entityType : EntityType) {
        weapon = Weapon(name: weaponName)
        self.originalSize = colliderSize
        self.originalFeetSize = feetHitSize
        var texture = SKTexture(imageNamed: imageName)
        desiredPosition = CGPointZero
        texture.filteringMode = SKTextureFilteringMode.Nearest
        DEFAULT_SIZE = texture.size()
        jumping = SKTexture(textureWithoutAA: "BreadieJump0")
        walking = LoopingAnimation(spriteName: "BreadieWalk", amountOfSprites: 14, delay: 0.08)
        idle = SKTexture(textureWithoutAA: "BreadieIdle0")
        type = entityType
        super.init(texture: texture, color: UIColor.whiteColor(), size: texture.size())
        addChild(weapon!)
    }
    
    init(info : String) {
        var infoBits = info.componentsSeparatedByString(":")
        //Remove spaces
        for i in 0..<infoBits.count {
            infoBits[i].remove(NSCharacterSet.whitespaceCharacterSet())
        }
        
        let name = infoBits[0]
        let positionBits = infoBits[1].componentsSeparatedByString(",")
        let position = CGPoint(x: positionBits[0].toDouble(), y: positionBits[1].toDouble())
        let cSizeBits = infoBits[2].componentsSeparatedByString(",")
        let cSize = CGSize(width: cSizeBits[0].toDouble(), height: cSizeBits[1].toDouble())
        let cFeetSizeBits = infoBits[3].componentsSeparatedByString(",")
        let cFeetSize = CGSize(width: cFeetSizeBits[0].toDouble(), height: cFeetSizeBits[1].toDouble())
        let type = EntityType.fromRaw(infoBits[4].toInt()!)
        let sizeMultiplier = infoBits[5].toInt()!
        
        self.sizeMultiplier = sizeMultiplier
        
        self.originalSize = cSize
        self.originalFeetSize = cFeetSize
        desiredPosition = CGPointZero
        jumping = SKTexture(textureWithoutAA: "\(name)Jump0")
        walking = LoopingAnimation(spriteName: "\(name)Walk", amountOfSprites: 14, delay: 0.08)
        idle = SKTexture(textureWithoutAA: "\(name)Idle0")
        DEFAULT_SIZE = idle.size()
        self.type = type!
        super.init(texture: idle, color: UIColor.whiteColor(), size: idle.size())
        anchorPoint = CGPointZero
        self.position = position
        self.name = name
        if infoBits.count > 5 {
            weapon = Weapon(name: infoBits[6])
            addChild(weapon!)
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    var colliderSize : CGSize {
        get {
            return originalSize * CGFloat(sizeMultiplier)
        }
    }
    
    var colliderFeetHitBoxSize : CGSize {
        get {
            return originalFeetSize * CGFloat(sizeMultiplier)
        }
    }
    
    var colliderFrame : CGRect {
        get {
            if xScale.isNegative {
                return CGRect(origin: self.reversedPosition, size: colliderSize)
            } else {
                return CGRect(origin: self.position, size: colliderSize)
            }
        }
    }
    
    var colliderFeetHitbox : CGRect {
        get {
            if xScale.isNegative {
                var positionFromLeft = (colliderFrame.width - colliderFeetHitBoxSize.width) / 2
                return CGRect(origin: CGPoint(x: reversedPosition.x + positionFromLeft, y: position.y), size: colliderFeetHitBoxSize)
            } else {
                var positionFromLeft = (colliderFrame.width - colliderFeetHitBoxSize.width) / 2
                return CGRect(origin: CGPoint(x: position.x + positionFromLeft, y: position.y), size: colliderFeetHitBoxSize)
            }
        }
    }
    
    var sizeMultiplier : Int = 1
    
    var boundingBox : CGRect {
        get {
            var box = self.colliderFrame
            box.inset(dx: 2 * CGFloat(sizeMultiplier), dy: 0)
            var diff = self.desiredPosition - self.position
            box.inset(dx: diff.x, dy: diff.y)
            return box
        }
    }
    
    var feetHitBox : CGRect {
        get {
            var box = self.colliderFeetHitbox
            var diff = self.desiredPosition - self.position
            box.inset(dx: diff.x, dy: diff.y)
            return box
        }
    }
    
    var rightLineSensor : CGRect {
        get {
            var top = self.boundingBox.topRight.y
            var bottom = self.boundingBox.bottomRight.y + CGFloat(sizeMultiplier) * maxSlopeStepUp
            var right = self.boundingBox.bottomRight.x
            var left = self.boundingBox.bottomRight.x - 4 * CGFloat(sizeMultiplier)
            
            return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
        }
    }
    
    var leftLineSensor : CGRect {
        get {
            var top = self.boundingBox.topRight.y
            var bottom = self.boundingBox.bottomRight.y + CGFloat(sizeMultiplier) * maxSlopeStepUp
            var left = self.boundingBox.origin.x
            var right = self.boundingBox.origin.x + 4 * CGFloat(sizeMultiplier)
            
            return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
        }
    }
    
    var fullRightLineSensor : CGRect {
        get {
            var top = self.boundingBox.topRight.y - 1
            var bottom = self.position.y
            var right = self.boundingBox.bottomRight.x
            var left = self.boundingBox.bottomRight.x - 4 * CGFloat(sizeMultiplier)
            
            return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
        }
    }
    
    var fullLeftLineSensor : CGRect {
        get {
            var top = self.boundingBox.topRight.y - 1
            var bottom = self.position.y
            var left = self.boundingBox.origin.x
            var right = self.boundingBox.origin.x + 4 * CGFloat(sizeMultiplier)
            
            return CGRect(x: left, y: bottom, width: right - left, height: top - bottom)
        }
    }
    
    var topRightSensor : CGPoint {
        get {
            return CGPoint(x: feetHitBox.topRight.x - CGFloat(sizeMultiplier), y: boundingBox.topRight.y)
        }
    }
    
    var topLeftSensor : CGPoint {
        get {
            return CGPoint(x: feetHitBox.topLeft.x + CGFloat(sizeMultiplier), y: boundingBox.topLeft.y)
        }
    }
    
    var bottomRightSensor : CGPoint {
        get {
            return self.feetHitBox.bottomRight
        }
    }
    
    var bottomLeftSensor : CGPoint {
        get {
            return self.feetHitBox.origin
        }
    }
    
    var leftSlopeSensor : CGPoint {
        get {
            return bottomLeftSensor + CGPoint(x: 0, y: maxSlopeStepUp * CGFloat(sizeMultiplier)) + 1
        }
    }
    
    var rightSlopeSensor : CGPoint {
        get {
            return bottomRightSensor + CGPoint(x: 0, y: maxSlopeStepUp * CGFloat(sizeMultiplier)) + 1
        }
    }
    
    var reversedPosition : CGPoint {
        get {
            return CGPoint(x: position.x - self.size.width, y: position.y)
        }
    }
    
    func rightLineSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> LineSensor {
        var highPoint = Int(floor(rightLineSensor.topRight.y / CGFloat(tileSize)))
        var lowPoint = Int(floor(rightLineSensor.bottomRight.y / CGFloat(tileSize)))
        var column = Int(floor(rightLineSensor.bottomRight.x / CGFloat(tileSize)))
        var t : [Tile] = []
        if column < tiles.count && ((highPoint < tiles.count) && (lowPoint > 0)) {
            for x in lowPoint...highPoint {
                t.append(tiles[column][x])
            }
        } else {
            for x in lowPoint...highPoint {
                t.append(Tile(id: 0, isSolid: false, isClimbable: false))
            }
        }
        return LineSensor(detectedTiles: t)
        
    }
    
    func leftLineSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> LineSensor {
        var highPoint = Int(floor(leftLineSensor.topRight.y / CGFloat(tileSize)))
        var lowPoint = Int(floor(leftLineSensor.bottomRight.y / CGFloat(tileSize)))
        var column = Int(floor(leftLineSensor.bottomLeft.x / CGFloat(tileSize)))
        var t : [Tile] = []
        if column < tiles.count && ((highPoint < tiles.count) && (lowPoint > 0)) {
            for x in lowPoint...highPoint {
                t.append(tiles[column][x])
            }
        } else {
            for x in lowPoint...highPoint {
                t.append(Tile(id: 0, isSolid: false, isClimbable: false))
            }
        }
        return LineSensor(detectedTiles: t)
    }
    
    func topLeftSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (topLeftSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func topRightSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (topRightSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func bottomRightSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (bottomRightSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func bottomLeftSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (bottomLeftSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func leftSlopeSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (leftSlopeSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func rightSlopeSensor(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> Tile {
        var pointInFrame = (rightSlopeSensor - origin)
        var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
        var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
        if tiles.contains(x) && tiles[0].contains(y) {
            return tiles[x][y]
        } else {
            return Tile(id: 0, isSolid: false, isClimbable: false)
        }
    }
    
    func allSensors(tiles : [[Tile]], tileSize : Int, origin : CGPoint) -> (Tile, Tile, Tile, Tile, LineSensor, LineSensor, Tile, Tile) {
        var tl = topLeftSensor(tiles, tileSize: tileSize, origin: origin)
        var tr = topRightSensor(tiles, tileSize: tileSize, origin: origin)
        var bl = bottomLeftSensor(tiles, tileSize: tileSize, origin: origin)
        var br = bottomRightSensor(tiles, tileSize: tileSize, origin: origin)
        var ll = leftLineSensor(tiles, tileSize: tileSize, origin: origin)
        var rl = rightLineSensor(tiles, tileSize: tileSize, origin: origin)
        var rss = rightSlopeSensor(tiles, tileSize: tileSize, origin: origin)
        var lss = leftSlopeSensor(tiles, tileSize: tileSize, origin: origin)
        
        return (tl, tr, bl, br, ll, rl, lss, rss)
    }
    
    func moveTo(location : CGPoint, speed : Double) {
        var distance = Double(sqrt(pow((self.position.x - location.x), 2) + pow(self.position.y - location.y, 2)))
        
        var motion = SKAction.moveTo(location, duration: speed / distance)
        self.runAction(motion)
    }
    
    func moveTo(location : CGPoint, time : Double) {
        var motion = SKAction.moveTo(location, duration: time)
        self.runAction(motion)
    }
    
    func doesCollideWith(other : CGRect) -> Bool {
        return self.frame.intersects(other)
    }
    
    func doesCollideWith(other : Entity) -> Bool {
        return self.frame.intersects(other.frame)
    }
    
    func jump() {
        if !isJumping {
            jumpVelocity = jumpStrength
            isJumping = true
        }
    }
    
    func applyForce(force : CGPoint) {
        velocity += force
    }
    
    func update(delta : NSTimeInterval) {
        
        //velocity.y.cap(-terminalVelocity)
        
        switch(currentDirection) {
        case Directions.Right:
            velocity = CGPoint(x: walkingSpeed, y: velocity.y)
            lookRight()
        case Directions.Left:
            velocity = CGPoint(x: -walkingSpeed, y: velocity.y)
            lookLeft()
        case Directions.None:
            walking.timer = 0
            velocity = CGPoint(x: velocity.x * CGFloat(delta) * friction, y: velocity.y)
        default:
            velocity = CGPoint(x: velocity.x * CGFloat(delta) * friction, y: velocity.y)
        }
        
        if velocity.x > 10 {
            walking.timer += delta
            //idle.timer = 0
            texture = walking.currentTexture
        } else if velocity.x < -10 {
            walking.timer += delta
            //idle.timer = 0
            texture = walking.currentTexture
        } else {
            texture = idle
            //idle.timer += delta
            //texture = idle.currentTexture
        }
        
        if (hasMass) {
            if jumpVelocity.y > 0 {
                jumpTimer += delta
                //jumping.timer += delta
                //texture = jumping.currentTexture
                jumpVelocity += (GRAVITYPULL * (CGFloat(jumpTimer) / 100))
                velocity.y = jumpVelocity.y
            } else {
                velocity += GRAVITYPULL * CGFloat(delta)
                jumpVelocity = CGPointZero
                jumpTimer = 0
                //jumping.timer = 0
            }
        }
        
        velocity += externalVelocities * CGFloat(delta)
        
        if isJumping {
            texture = jumping
        }
        var newSize = texture!.size() * CGFloat(sizeMultiplier)
        if xScale.isNegative {
            newSize.width = -newSize.width
        }
        size = newSize
        
        if weapon != nil {
            weapon!.update(delta)
            weapon!.size = weapon!.currentCollider.currentTexture.size() * CGFloat(sizeMultiplier)
        }
        
        self.desiredPosition = position
        self.desiredPosition += velocity * CGFloat(delta)
    }
    
    func hitGround() {
        velocity = CGPoint(x: velocity.x, y: 0)
        isJumping = false
        //jumping.timer = 0
    }
    
    func cancelJump() {
        //jumping.timer = 0
        jumpVelocity = CGPointZero
        jumpTimer = 0
        velocity.y -= jumpStrength.y / 10
    }
    
    func lookRight() {
        if xScale == -1 {
            xScale = 1
            position.x -= originalSize.width * CGFloat(sizeMultiplier)
        } else {
            xScale = 1
        }
    }
    
    func lookLeft() {
        if xScale == 1 {
            xScale = -1
            position.x += originalSize.width * CGFloat(sizeMultiplier)
        } else {
            xScale = -1
        }
    }
}