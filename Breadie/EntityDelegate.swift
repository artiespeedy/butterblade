//
//  CollisionDelegate.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/3/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class EntityDelegate: SKNode {
    var entities : [Entity]
    var map : Map
    var mapOrigin : CGPoint
    var tileSize : Int
    
    var mainEntity : Entity?
    
    override init() {
        self.map = Map()
        entities = []
        tileSize = map.tileSize
        mapOrigin = map.frame.origin
        super.init()
        for i in entities {
            if i.type == EntityType.Main {
                mainEntity = i
            }
        }
    }
    
    init(map : Map) {
        entities = []
        
        self.map = map
        tileSize = map.tileSize
        mapOrigin = map.frame.origin
        
        super.init()
        for i in entities {
            if i.type == EntityType.Main {
                mainEntity = i
            }
        }
    }
    
    init(entity : Entity, map : Map) {
        entities = []
        entities.append(entity)
        
        self.map = map
        tileSize = map.tileSize
        mapOrigin = map.frame.origin
        
        super.init()
        addChild(entity)
        for i in entities {
            if i.type == EntityType.Main {
                mainEntity = i
            }
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    func checkAndResolveCollisionsForEntities(delta : CGFloat) {
        for i in entities {
            i.update(NSTimeInterval(delta))
            
            i.externalVelocities = CGPointZero
            
            var newPosition = i.desiredPosition
            
            //0 - Top Left
            //1 - Top Right
            //2 - Bottom left
            //3 - Bottom Right
            //4 - Left line
            //5 - Right line
            //6 - Left slope
            //7 - Right slope
            var allSensors = i.allSensors(map.tiles, tileSize: tileSize, origin: mapOrigin)
            
            //GRAVITY
            
            var leftSensorHeight = allSensors.2.heightAt(i.feetHitBox.bottomLeft.x)
            var rightSensorHeight = allSensors.3.heightAt(i.feetHitBox.bottomRight.x)
            
            var groundHeight: CGFloat = 0
            
            //If the ground is sloped
            if allSensors.2.isSloped || allSensors.3.isSloped {
                //And is headed right
                if (allSensors.2.direction == Directions.Right) || (allSensors.3.direction == Directions.Right) {
                    groundHeight = rightSensorHeight
                //Or is headed left
                } else if (allSensors.2.direction == Directions.Left) || (allSensors.3.direction == Directions.Left) {
                    groundHeight = rightSensorHeight
                } else {
                    //Go with whichever foot is higher
                    if leftSensorHeight > rightSensorHeight {
                        groundHeight = leftSensorHeight
                    } else {
                        groundHeight = rightSensorHeight
                    }
                }
            } else {
                //Go with which ever is higher
                if leftSensorHeight > rightSensorHeight {
                    groundHeight = leftSensorHeight
                } else {
                    groundHeight = rightSensorHeight
                }
                
                //Check whether or not there is a slope above the feet
                var leftSlopeHeight = allSensors.6.heightAt(i.feetHitBox.bottomLeft.x)
                var rightSlopeHeight = allSensors.7.heightAt(i.feetHitBox.bottomRight.x)
                
                //If there is, move up to it
                if (allSensors.6.isSloped && allSensors.6.isSolid) || (allSensors.7.isSloped && allSensors.7.isSolid) {
                    if leftSlopeHeight > rightSlopeHeight {
                        groundHeight = rightSlopeHeight
                    } else {
                        groundHeight = rightSlopeHeight
                    }
                }
            }
            
            //If there is a ground
            if !allSensors.2.isSolid && !allSensors.3.isSolid {
                groundHeight = 0
                var leftSlopeHeight = allSensors.6.heightAt(i.feetHitBox.bottomLeft.x)
                var rightSlopeHeight = allSensors.7.heightAt(i.feetHitBox.bottomRight.x)
                
                //See if there are slopes in the feets and move to it
                if (allSensors.6.isSloped && allSensors.6.isSolid) || (allSensors.7.isSloped && allSensors.7.isSolid) {
                    if leftSlopeHeight > rightSlopeHeight {
                        groundHeight = leftSlopeHeight
                    } else {
                        groundHeight = rightSlopeHeight
                    }
                }
            }
            
            //Bug here
            //Make line sensor get more data
            //check the does collide function
            //Check to see if it collides with anything to the left
            if (allSensors.4.doesCollideSolid(i.fullLeftLineSensor)) {
                if i.velocity.x < 0 {
                    newPosition.x = i.position.x
                }
                if i.currentDirection == Directions.Left {
                    if allSensors.4.isClimable {
                        i.velocity = CGPoint(x: 0, y: 100)
                    }
                } else {
                    if allSensors.4.isClimable {
                        i.velocity = CGPoint(x: 0, y: -50)
                    }
                }
            }
            
            //Check to see if it collides with anything to the right
            if (allSensors.5.doesCollideSolid(i.fullRightLineSensor)) {
                if i.velocity.x > 0 {
                    newPosition.x = i.position.x
                }
                if i.currentDirection == Directions.Right {
                    if allSensors.5.isClimable {
                        i.velocity = CGPoint(x: 0, y: 100)
                    }
                } else {
                    if allSensors.5.isClimable {
                        i.velocity = CGPoint(x: 0, y: -50)
                    }
                }
            }
            
            //check groundHeight afterward
            if groundHeight - i.desiredPosition.y > 0 {
                newPosition = CGPoint(x: newPosition.x, y: groundHeight)
                i.hitGround()
            }
            
            //Check height
            if allSensors.0.isSolid || allSensors.1.isSolid {
                if i.velocity.y > 0 {
                    newPosition.y = i.position.y
                    i.cancelJump()
                }
            }
            
            i.position = newPosition
            i.currentDirection = Directions.None
        }
    }
    
    func addEntity(toAdd : Entity) {
        entities.append(toAdd)
        addChild(toAdd)
        for i in entities {
            if i.type == EntityType.Main {
                mainEntity = i
            }
        }
    }
    
    func addVelocityToEntityNamed(name : String, velocity : CGPoint) {
        var entity = self.childNodeWithName(name) as Entity
        entity.applyForce(velocity)
    }
    
    func getEntity(name : String) -> Entity {
        return self.childNodeWithName(name) as Entity
    }
    
    func refresh() {
        self.removeAllChildren()
        for i in entities {
            addChild(i)
        }
        tileSize = map.tileSize
        mapOrigin = map.frame.origin
    }
}