//
//  EntityStateController.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/21/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class EntityStateController {
    let entityName : String
    var stateEngine : StateEngine<String>
    init(entityName : String) {
        self.entityName = entityName
        let idle = LoopingAnimation(spriteName: "BreadieIdle", amountOfSprites: 2, delay: 100)
        let walking = LoopingAnimation(spriteName: "BreadieWalk", amountOfSprites: 14, delay: 50)
        let strideLeft = Animation(spriteName: "BreadieWalk", amountOfSprites: 7, delay: 50)
        let strideRight = Animation(spriteName: "BreadieWalk", range: 7...13, delay: 50)
        stateEngine = StateEngine<String>(stateKey: "Idle", firstState: idle)
        stateEngine.addState("Walking", state: walking)
        stateEngine.addState("StrideLeft", state: strideLeft)
        stateEngine.addState("StrideRight", state: strideRight)
    }
}