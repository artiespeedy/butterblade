//
//  Extensions.swift
//  Breadie
//
//  Created by Alex Mai on 7/25/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

extension Int {
    var isNegative : Bool {
    get {
        if (self==0) {
            return false
        } else {
            return -1==(self / abs(self))
        }
    }
    }
    
    func isolate(index : Int) -> Int? {
        let dCount = digitCount()
        let distanceToDigit = Int(pow(10, Double((dCount - index))))
        if distanceToDigit < 1 {
            return nil
        } else {
            let isolationDivider = distanceToDigit * 10
            return self / distanceToDigit - self / isolationDivider * 10
        }
    }
    
    func digitCount() -> Int {
        var count : Int
        (self == 0) ? (count = 1) : (count = Int(log10(Double(self))) + 1)
        return count
    }
}

extension SKSpriteNode {
    convenience init(imageWithoutAA : String) {
        var texture = SKTexture(imageNamed: imageWithoutAA)
        texture.filteringMode = SKTextureFilteringMode.Nearest
        self.init(texture: texture)
    }
}

extension SKTexture {
    convenience init(textureWithoutAA : String) {
        self.init(imageNamed: textureWithoutAA)
        self.filteringMode = SKTextureFilteringMode.Nearest
    }
}

extension Array {
    var stringValue : String {
    get {
        var string = ""
        for x in 0..<self.count {
            string + "\(self[x])"
        }
        return string
    }
    }
    
    func executeOnAll<T: AnyObject>(function : (T) -> () -> ()) {
        for i in self {
            function(i as T)()
        }
    }
    
    func executeOnAllWithParameter<T: AnyObject, P>(function : (T) -> (P) -> (), parameter : P) {
        for i in self {
            function(i as T)(parameter as P)
        }
    }
    
    func contains(number : Int) -> Bool {
        return (number >= 0) && (number < self.count)
    }
    
    static func new2D<T: AnyObject>(width : Int, height : Int, defaultValue : T) -> [[T]] {
        return [[T]](count: width, repeatedValue: [T](count: height, repeatedValue: defaultValue))
    }
}

extension String {
    func toFloat() -> Float {
        return (self as NSString).floatValue
    }
    
    func toDouble() -> Double {
        return (self as NSString).doubleValue
    }
    
    static func loadFromFile(name : String, ext : String) -> String? {
        let path = NSBundle.mainBundle().pathForResource(name, ofType: ext)
        if path != nil {
            let output = String.stringWithContentsOfFile(path!, encoding: NSUTF8StringEncoding, error: nil)
            return output
        } else {
            return nil
        }
    }

    subscript (i : Int) -> String {
        return String(Array(self)[i])
    }

    mutating func remove(set : NSCharacterSet) {
        let range = self.rangeOfCharacterFromSet(set)
        if let r = range {
            self.removeRange(r)
        }
        if self.doesContain(set) {
            self.remove(set)
        }
    }

    mutating func removeFirst(set : NSCharacterSet) {
        let range = self.rangeOfCharacterFromSet(set)
        if let r = range {
            self.removeRange(r)
        }
    }
    
    func doesContain(set : NSCharacterSet) -> Bool {
        let range = self.rangeOfCharacterFromSet(set)
        if let r = range {
            return true
        }
        return false
    }
}

