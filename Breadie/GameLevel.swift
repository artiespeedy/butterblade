//
//  GameLevel.swift
//  Breadie
//
//  Created by Alex Mai on 7/27/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let TILESIZE = 16

class GameLevel : SKSpriteNode {
    
    var tSize = CGSize(width: TILESIZE, height: TILESIZE)
    var tMultiplier : CGFloat = 1
    
    var data : [[Int]]
    var backgroundColor = SKColor.blueColor()
    var fileContent : String = ""
    
    init(levelData : [[Int]], skyTexture : SKTexture) {
        data = levelData
        super.init(texture: skyTexture, color: backgroundColor, size: self.pixelLevelSize)
        refresh()
    }
    
    init(levelName : String, skyTexture : SKTexture) {
        data = [[]]
        super.init(texture: skyTexture, color: backgroundColor, size: CGSize(width: 0, height: 0))
        
        data = loadLevelFromText(levelName, type: "txt")!
        size = self.pixelLevelSize
        print(size)
        refresh()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported!")
    }
    
    var actualTileSize : Int {
    get {
        return TILESIZE * Int(tileSizeMultiplier)
    }
    }
    
    var sizeActual : CGSize {
    get {
        return CGSize(width: self.pixelWidth, height: self.pixelHeight)
    }
    }
    
    var frameActual : CGRect {
    get {
        return CGRect(origin: CGPoint(x: self.position.x, y: self.position.y - 110), size: self.sizeActual)
    }
    }
    
    var height : Int {
    get {
        return data[0].count
    }
    }
    
    var pixelHeight : Int {
    get {
        return self.height * TILESIZE * Int(tileSizeMultiplier)
    }
    }
    
    var width : Int {
    get {
        return data.count
    }
    }
    
    var pixelWidth : Int {
    get {
        return self.width * TILESIZE * Int(tileSizeMultiplier)
    }
    }
    
    var sprites : [[SKSpriteNode]] {
    get {
        var skyTile = SKSpriteNode(imageWithoutAA: "Tile 0.png")
        var t = [[SKSpriteNode]](count: self.width, repeatedValue: [SKSpriteNode](count: self.height, repeatedValue: skyTile))
        for x in 0..<self.width {
            for y in 0..<self.height {
                var sprite = SKSpriteNode(imageWithoutAA: "Tile \(data[x][y]).png")
                sprite.position = CGPoint(x: x * TILESIZE * Int(tMultiplier), y: y * TILESIZE * Int(tMultiplier)) + CGPoint(x: TILESIZE * Int(tileSizeMultiplier) / 2, y: TILESIZE * Int(tileSizeMultiplier) / 2)
                sprite.size = tSize
                if t[x][y] == 0 {
                    sprite.color = UIColor.blueColor()
                    sprite.colorBlendFactor = 0.5
                }
                t[x][y] = sprite
            }
        }
        return t
    }
    }
    
    var rect : CGRect {
    get {
        return CGRect(x: 0, y: 0, width: self.width, height: self.height)
    }
    }
    
    var levelSize : CGSize {
    get {
        return CGSize(width: self.width, height: self.height)
    }
    }
    
    var pixelLevelSize : CGSize {
    get {
        return CGSize(width: self.width * TILESIZE * Int(tileSizeMultiplier), height: self.height * TILESIZE * Int(tileSizeMultiplier))
    }
    }
    
    var tileSizeMultiplier : CGFloat {
    get {
        return tMultiplier
    }
    set (m) {
        tMultiplier = m
        tSize *= tMultiplier
        refresh()
    }
    }
    
    func getTile(point : CGPoint) -> Int {
        return data[Int(point.x)][Int(point.y)]
    }
    
    func getTiles(rect : CGRect) -> ([[Int]])? {
        
        var r = CGRect(x: rect.origin.x, y: rect.origin.y - 110, width: rect.width, height: rect.height)
        
        if self.frameActual.contains(r) {
            
            var tx = Int(round(rect.origin.x)) / (TILESIZE * Int(tileSizeMultiplier))
            var ty = Int(round(rect.origin.y - 110)) / (TILESIZE * Int(tileSizeMultiplier))
            var twidth = Int(ceil(rect.width)) / (TILESIZE * Int(tileSizeMultiplier))
            var theight = Int(rect.height) / (TILESIZE * Int(tileSizeMultiplier))
            
            println("\(rect.origin.x), \(rect.origin.y), \(rect.width), \(rect.height)")
            println("\(tx), \(ty), \(twidth), \(rect.height/32)")
            
            var tiles = self.sprites
            
            var width = 0, height = 0
            
            /*for x in 0..<self.width {
                for y in 0..<self.height {
                    if collideRect.intersects(tiles[x][y].frame) {
                        width++
                        print("\(x), \(y)")
                        if y - ty > height - 1 {
                            height = y - ty + 1
                        }
                    }
                }
            }*/
            return nil
        } else {
            return nil
        }
        
        /*if (true) {
            var t = [[Int]](count: Int(width), repeatedValue: [Int](count: Int(height), repeatedValue: 0))
            var allTiles = self.sprites
            
            for xx in x..<width {
                for yy in y..<height {
                    t[Int(xx)][Int(yy)] = data[Int(xx)][Int(yy)]
                }
            }
            return t
        } else {
            return nil
        }*/
    }
    
    func getTileFromCoordsInLayer(point : CGPoint) {
        
    }
    
    func getTilesSurroundingEntity(entity : Entity) {
        
    }
    
    func loadLevelFromText(name : String, type : String) -> ([[Int]])? {
        let path = NSBundle.mainBundle().pathForResource(name, ofType: type)
        var possibleContent = String.stringWithContentsOfFile(path!, encoding: NSUTF8StringEncoding, error: nil)
        if let content = possibleContent {
            fileContent = possibleContent!
            var splitContent = fileContent.componentsSeparatedByString(";")
            
            var rows = splitContent[0].componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            var rowsSplit : [[String]] = []
            for i in rows.reverse() {
                rowsSplit.append(i.componentsSeparatedByString(" "))
            }
            
            var levelData = [[Int]](count: rowsSplit[0].count, repeatedValue: [Int](count: rowsSplit.count, repeatedValue: 0))
            
            for x in 0..<rowsSplit[0].count {
                for y in 0..<rowsSplit.count {
                    levelData[x][y] = rowsSplit[y][x].toInt()!
                }
            }
            
            return levelData
            
        } else {
            print("Could not load level; level not found")
        }
        return nil
    }
    
    func refresh() {
        self.removeAllChildren()
        var tiles = self.sprites
        for x in 0..<self.width {
            for y in 0..<self.height {
                self.addChild(tiles[x][y])
            }
        }
    }
}