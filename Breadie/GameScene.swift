//
//  GameScene.swift
//  Breadie
//
//  Created by Eric Mai on 7/15/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var sender : UIViewController?
    
    var previousTime : NSTimeInterval?

    var points = [CGPoint(x: 0, y: 0)]
    
    var buttonManager : SKButtonDelegate = SKButtonDelegate()
    
    var viewCamera : Camera
    
    init(levelName : String) {
        println("Loading \(levelName)")
        viewCamera = Camera()
        super.init(size: sceneFrame.size)
        anchorPoint = CGPointZero
        viewCamera = Camera(view: self.frame)
        viewCamera.position = CGPoint(x: 0, y: 0)
        var pLevel = String.loadFromFile(levelName, ext: "level")
        
        var m : Map
        var e : EntityDelegate
        
        if let level = pLevel {
            var levelComponents : [[String]] = []
            let components = level.componentsSeparatedByString("|")
            for i in 0..<components.count {
                var c = components[i]
                let pInfoLines = c.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
                var infoLines : [String] = []
                for x in pInfoLines {
                    if x.doesContain(NSCharacterSet.alphanumericCharacterSet()) {
                        infoLines.append(x)
                    }
                }
                //Info lines now equals the lines of content in the file
                
                levelComponents.append(infoLines)
            }
            
            var info : [[String]] = []
            for line in levelComponents[0] {
                info.append(line.componentsSeparatedByString("-"))
            }
            m = Map(content: info)
            m.tileSizeMultiplier = 6
            
            e = EntityDelegate(map: m)
            
            for i in levelComponents[1] {
                var en = Entity(info: i)
                e.addEntity(en)
            }
            
            viewCamera.map = m
            viewCamera.entities = e
        } else {
            fatalError("Failed to load level.")
        }
        
        setupButtons()
        addChild(viewCamera)
    }

    required init(coder aDecoder: NSCoder) {
        viewCamera = Camera()
        super.init(coder: aDecoder)
        
    }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        self.anchorPoint = CGPointZero
        /*
        //Entity setup
        var char = Entity(imageName: "BreadieSprite1", weaponName: "Butterblade", colliderSize: CGSize(width: 24, height: 24), feetHitSize: CGSize(width: 8, height: 24))
        char.anchorPoint = CGPointZero
        char.position = CGPoint(x: 100, y: 400)
        char.sizeMultiplier = 4
        char.hasMass = true
        char.name = "Main"
        char.walkingSpeed = 80
        
        entityController = EntityDelegate(entity: char, map: map)
        
        //Camera Setup
        viewCamera = Camera(objects: [map], view: self.frame, viewOffset: 200)
        
        viewCamera.addChild(entityController)
        viewCamera.objects.append(entityController)
        viewCamera.position = CGPoint(x: 0, y: 95)
        viewCamera.zPosition = 0.2
        
        self.addChild(viewCamera)*/
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        points = [touches.allObjects[0].locationInNode(self.scene)]
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        points.append(touches.allObjects[0].locationInNode(self.scene))
        var gesture = Gesture(points: points)
        var move = gesture.toMove()
        viewCamera.entities!.mainEntity!.weapon!.currentMove = move
    }
   
    override func update(currentTime: CFTimeInterval) {
        if let i = previousTime {
            
        } else {
            self.previousTime = currentTime
        }
        
        var delta = CGFloat(currentTime - previousTime!)
        if delta > 0.02 {
            delta = 0.02
        }
        
        viewCamera.entities!.checkAndResolveCollisionsForEntities(delta)
        
        buttonManager.updateAll(delta)
        
        viewCamera.centerOnMain()
    }
    
    func findButtonsTouched(touches : NSSet) {
        
    }
    
    func paintPoint(point : CGPoint) {
        var touchPoint = SKShapeNode(ellipseInRect: CGRect(origin: CGPoint(x: Int(point.x) - TPWIDTH/2, y: Int(point.y) - TPHEIGHT/2), size: CGSize(width: TPWIDTH, height: TPHEIGHT)))
        self.addChild(touchPoint)
    }
    
    func setupButtons() {
        var moveUp = {() -> () in
            self.viewCamera.entities!.mainEntity!.jump()
        }
        
        var moveRight = {() -> () in
            self.viewCamera.entities!.mainEntity!.currentDirection = Directions.Right
        }
        
        var moveLeft = {() -> () in
            self.viewCamera.entities!.mainEntity!.currentDirection = Directions.Left
        }
        
        var goBack = {() -> () in
            self.buttonManager.unpressAll()
            self.removeFromParent()
            self.sender!.performSegueWithIdentifier("BackFromGame", sender: self)
        }
        
        println(sceneFrame)
        
        //Buttons
        var up = SKButton(pressedTexture: SKTexture(textureWithoutAA: "Jump"), unpressedTexture: SKTexture(textureWithoutAA: "uJump"), functionToCall: moveUp)
        up.position = ScreenPositions.BottomRight.positionInScene + CGPoint(x: -10 - 75, y: 10)
        //up.position = CGPoint(x: 1024, y: 600) + CGPoint(x: -10 - 75, y: -10)
        up.size = CGSize(width: 75, height: 75)
        up.zPosition = 1
        
        var right = SKButton(pressedTexture: SKTexture(textureWithoutAA: "Right"), unpressedTexture: SKTexture(textureWithoutAA: "uRight"), functionToCall: moveRight)
        right.position = ScreenPositions.BottomLeft.positionInScene + CGPoint(x: 20 + 75, y: 10)
        right.size = CGSize(width: 75, height: 75)
        right.zPosition = 1
        
        var left = SKButton(pressedTexture: SKTexture(textureWithoutAA: "Left"), unpressedTexture: SKTexture(textureWithoutAA: "uLeft"), functionToCall: moveLeft)
        left.position = ScreenPositions.BottomLeft.positionInScene + CGPoint(x: 10, y: 10)
        left.size = CGSize(width: 75, height: 75)
        left.zPosition = 1
        
        var back = SKButton(pressedTexture: SKTexture(textureWithoutAA: "Left"), unpressedTexture: SKTexture(textureWithoutAA: "uLeft"), functionToCall: goBack)
        back.position = ScreenPositions.TopRight.positionInScene + CGPoint(x: -10 - 75, y: -10 - 75)
        back.size = CGSize(width: 75, height: 75)
        back.zPosition = 1
        back.doesLoop = false
        
        //Add buttons to manager
        buttonManager.addButton(up)
        buttonManager.addButton(left)
        buttonManager.addButton(right)
        buttonManager.addButton(back)
        
        addChild(buttonManager)
    }
}