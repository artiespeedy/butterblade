//
//  GameViewController.swift
//  Breadie
//
//  Created by Eric Mai on 7/15/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import UIKit
import SpriteKit

var scenes = [String: GameScene]()
var sceneFrame = CGRect(x: 0, y: 0, width: 1024, height: 768)

class GameViewController: UIViewController {
    
    var currentScene : String?
    
    var currentDisplayedScene : GameScene {
        get {
            return currentView.scene as GameScene
        }
    }

    var currentView : SKView {
        get {
            return self.view as SKView
        }
    }
    
    func loadGameScene(levelName : String) {
        if scenes[levelName] == nil {
            var newScene = GameScene(levelName: levelName)
            newScene.sender = self
            scenes[levelName] = newScene
        }
        currentScene = levelName
        scenes[levelName]!.sender = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configure the view.
        
        let skView = currentView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.showsDrawCount = true
        skView.showsQuadCount = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        if currentScene != nil {
            var scene = scenes[currentScene!]!
            scene.scaleMode = .AspectFill
            scene.size = skView.bounds.size * 2
            currentView.presentScene(scene)
        }
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.toRaw())
        } else {
            return Int(UIInterfaceOrientationMask.All.toRaw())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
