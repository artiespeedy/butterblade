//
//  Gesture.swift
//  Breadie
//
//  Created by Alex Mai on 7/26/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let TPWIDTH = 10, TPHEIGHT = 10
let ARCHEIGHT = 40, LOWESTLINELENGTH : Int = 20, STRAIGHTLINESLOPE = 0.4, STRAIGHTUPMARGIN = 40 

enum GestureShape : Int {
    case Arc = 0
    case Line = 1
    case None = 2
    
    func toStringRaw() -> String {
        switch(self.toRaw()) {
        case 0:
            return "Arc"
        case 1:
            return "Line"
        case 2:
            return "None"
        default:
            return "Cool"
        }
    }
}

class Gesture {
    
    var shape : GestureShape, horizontalDirection : Directions, verticalDirection : Directions
    
    init(shape : GestureShape, horizontalDirection : Directions, verticalDirection : Directions) {
        self.shape = shape
        self.horizontalDirection = horizontalDirection
        self.verticalDirection = verticalDirection
    }
    
    init(points: [CGPoint]) {
        var midPoint = points[Int(points.count/2)]
        var startPoint = points[0]
        var endPoint = points[points.count-1]
        var highestPoint = startPoint
        for point in points {
            if point.y > highestPoint.y {
                highestPoint = CGPoint(x: point.x, y: point.y)
            }
        }
        
        var shape : GestureShape, horizontalDirection : Directions, verticalDirection : Directions
        
        //Find midpoint
        var midLine : CGFloat
        (startPoint.y > endPoint.y) ? (midLine = endPoint.y + (startPoint.y - endPoint.y) / 2) : (midLine = startPoint.y + (endPoint.y - startPoint.y) / 2)
        
        var centerLine : CGFloat
        (startPoint.x > endPoint.x) ? (centerLine = endPoint.x + (startPoint.x - endPoint.x) / 2) : (centerLine = startPoint.x + (endPoint.x - startPoint.x) / 2)
        
        var lengthOfLine = Int(sqrt(pow(startPoint.x - endPoint.x, 2) + pow(startPoint.y - endPoint.y, 2)))
        
        var slope = (endPoint.y - startPoint.y) / (endPoint.x - startPoint.y)
        var b = endPoint.y - (slope * endPoint.x)
        
        if ((Int(abs(midPoint.y - midLine)) > ARCHEIGHT) && ((startPoint.y > midPoint.y && endPoint.y > midPoint.y)||(startPoint.y < midPoint.y && endPoint.y < midPoint.y))) && (lengthOfLine > LOWESTLINELENGTH) {
            //Arcs
            shape = GestureShape.Arc
            
            //Vertical
            if Int(midPoint.y-midLine).isNegative {
                verticalDirection = Directions.Down
            } else {
                verticalDirection = Directions.Up
            }
            
            //Horizontal
            if Int(startPoint.x-centerLine).isNegative {
                horizontalDirection = Directions.Right
            } else {
                horizontalDirection = Directions.Left
            }
        } else if (lengthOfLine > LOWESTLINELENGTH) {
            //Line
            shape = GestureShape.Line
            
            //Vertical
            if(abs(slope) < CGFloat(STRAIGHTLINESLOPE)) {
                verticalDirection = Directions.None
            } else if(startPoint.y > endPoint.y) {
                verticalDirection = Directions.Down
            } else {
                verticalDirection = Directions.Up
            }
            
            //Horizontal
            if (Int(abs(startPoint.x - centerLine)) > STRAIGHTUPMARGIN) && Int(startPoint.x-centerLine).isNegative {
                horizontalDirection = Directions.Right
            } else if (Int(abs(startPoint.x - centerLine)) > STRAIGHTUPMARGIN) {
                horizontalDirection = Directions.Left
            } else {
                horizontalDirection = Directions.None
            }
        } else {
            shape = GestureShape.None
            verticalDirection = Directions.None
            horizontalDirection = Directions.None
        }
        
        self.shape = shape
        self.horizontalDirection = horizontalDirection
        self.verticalDirection = verticalDirection
    }
    
    func toMove() -> Move {
        switch((self.shape, self.horizontalDirection, self.verticalDirection)) {
        case (GestureShape.Line, Directions.None, _):
            return Move.Stab
        case (GestureShape.Line, _, Directions.None):
            return Move.Stab
        case (GestureShape.Line, _, Directions.Up):
            return Move.SliceUp
        case (GestureShape.Line, _, Directions.Down):
            return Move.SliceDown
        case (GestureShape.Arc, _, Directions.Up):
            return Move.OverHead
        case (GestureShape.Arc, _, Directions.Down):
            return Move.UpperCut
        default:
            return Move.None
        }
    }
}