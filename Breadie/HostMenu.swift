//
//  HostMenu.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/27/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import UIKit

class HostMenu : UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var players : UITableView!
    @IBOutlet var textfield : UITextField!
    let cellIdentifier = "Player"
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func viewDidLoad() {
        var selector : Selector = "resignFirstResponder"
        
        var gestureRecognizer = UITapGestureRecognizer(target: self.textfield, action: selector)
        gestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)! as UITableViewCell
        return cell
    }
}