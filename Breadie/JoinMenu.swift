//
//  JoinMenu.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/27/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import UIKit

class JoinMenu : UIViewController {

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet var servers : UITableView!
}