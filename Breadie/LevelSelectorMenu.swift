//
//  LevelSelectorMenu.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/22/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import UIKit

class LevelSelectorMenu: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellIdentifier = "Level"
    let nextSceneIdentifier = "LoadGame"
    var lastSelected : Int = 0
    
    var levelNames : [String]?
    
    @IBOutlet var tableView : UITableView!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        levelNames = []
        var manager = NSFileManager.defaultManager()
        var pPath = NSBundle.mainBundle().pathForResource("LevelTest", ofType: "level")
        var path = pPath?.stringByDeletingLastPathComponent
        var pNames = manager.contentsOfDirectoryAtPath(path!, error: nil)
        if let names = pNames {
            for n in names {
                var name = (n as String)
                var sName = name.componentsSeparatedByString(".")
                if sName[sName.count - 1] == "level" {
                    levelNames!.append(sName[0])
                }
            }
        }
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return levelNames!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var c: AnyObject! = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if let cell = c as? UITableViewCell {
            cell.textLabel?.text = levelNames![indexPath.item]
            return cell
        } else {
            var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            cell.textLabel?.text = levelNames![indexPath.item]
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        lastSelected = indexPath.item
        performSegueWithIdentifier(nextSceneIdentifier, sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var id = segue.identifier
        if id == nextSceneIdentifier {
            let nextScene = segue.destinationViewController as GameViewController
            sceneFrame.size = view.bounds.size * 2
            nextScene.loadGameScene(levelNames![lastSelected])
        }
    }
}