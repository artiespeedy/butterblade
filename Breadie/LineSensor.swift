//
//  LineSensor.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/13/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let maxSlopeClimb = 2

class LineSensor {
    var tiles : [Tile]
    
    init(detectedTiles : [Tile]) {
        tiles = detectedTiles
    }
    
    var topSlope : Tile {
        get {
            if doesContainSlopes != Directions.None {
                var slopes = slopedIndicies
                return tiles[slopes[slopes.count - 1]]
            } else {
                return Tile(id: 0, isSolid: false, isClimbable: false)
            }
        }
    }
    
    var slopedIndicies : [Int] {
        get {
            var slopes : [Int] = []
            for i in 0..<tiles.count {
                if tiles[i].isSloped {
                    slopes.append(i)
                }
            }
            return slopes
        }
    }
    
    var isClimable : Bool {
        get {
            for i in tiles {
                if i.isClimbable {
                    return true
                }
            }
            return false
        }
    }
    
    var doesContainSolids : Bool {
        get {
            for i in tiles {
                if i.isSolid {
                    return true
                }
            }
            return false
        }
    }
    
    var doesContainSlopes : Directions {
        get {
            for i in tiles {
                if i.isSloped {
                    return i.direction
                }
            }
            return Directions.None
        }
    }
    
    var leftEdge : CGFloat {
        get {
            return tiles[0].position.x
        }
    }
    
    var rightEdge : CGFloat {
        get {
            return tiles[0].position.x
        }
    }
    
    func distanceToLeftEdge(onXAxis : CGRect) -> CGFloat {
        //Is on right
        return onXAxis.topRight.x - leftEdge
    }
    
    func distanceToRightEdge(onXAxis : CGRect) -> CGFloat {
        //Is on left
        return onXAxis.origin.x - rightEdge
    }
    
    func doesCollideSolid(rectangle : CGRect) -> Bool {
        for i in tiles {
            if i.isSloped {
                if i.direction == Directions.Right {
                    return i.slopeRectForX(rectangle.origin.x).intersects(rectangle)
                } else {
                    return i.slopeRectForX(rectangle.topRight.x).intersects(rectangle)
                }
            } else {
                if i.frame.intersects(rectangle) && i.isSolid {
                    return true
                }
            }
        }
        return false
    }
    
    //doesCollideSlope, can go up
    func collideSlope(entity : Entity) -> (Bool, Bool) {
        if doesContainSlopes == Directions.None {
            var desiredHighpoint : Int = 0
            var currentHighpoint : Int = 0
            for i in slopedIndicies {
                var iDesiredHeight = tiles[i].slopeHeightAt(entity.desiredPosition.x)
                var iHeight = tiles[i].slopeHeightAt(entity.position.x)
                if iHeight > currentHighpoint {
                    currentHighpoint = iHeight
                }
                if iDesiredHeight > desiredHighpoint {
                    desiredHighpoint = iDesiredHeight
                }
            }
            
            if (desiredHighpoint - currentHighpoint) > (maxSlopeClimb) {
                return (true, true)
            } else {
                return (true, false)
            }
        }
        return (false, false)
    }
}