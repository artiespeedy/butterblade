//
//  Server.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/12/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class LocalAdvertiser : NSObject, MCNearbyServiceAdvertiserDelegate, UITableViewDataSource {
    var maxClients : Int
    var connectedClients : [Int] = []
    //var session : MCSession
    var peerID : MCPeerID
    var advertiser : MCNearbyServiceAdvertiser?
    let cellIdentifier = "Player"
    
    init(name : String, maxClients : Int) {
        peerID = MCPeerID(displayName: name)
        connectedClients = []
        //session = MCSession(peer: peerID)
        self.maxClients = maxClients
        super.init()
    }
    
    func startAdvertising() {
        if advertiser == nil {
            advertiser = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: nil, serviceType: "JoinGame")
            advertiser!.delegate = self
            advertiser!.startAdvertisingPeer()
            //advertiser = MCAdvertiserAssistant(serviceType: "JoinGame", discoveryInfo: nil, session: session)
            //advertiser!.start()
        }
    }
    
    func stopAdvertising() {
        if advertiser != nil {
            advertiser?.stopAdvertisingPeer()
            advertiser = nil
        }
    }
    
    /*func session(session: MCSession!, peer peerID: MCPeerID!, didChangeState state: MCSessionState) {
        
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!, fromPeer peerID: MCPeerID!) {
        
    }
    
    func session(session: MCSession!, didStartReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!) {
        
    }
    
    func session(session: MCSession!, didFinishReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, atURL localURL: NSURL!, withError error: NSError!) {
        
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!, withName streamName: String!, fromPeer peerID: MCPeerID!) {
        
    }*/
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)! as UITableViewCell
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser!, didNotStartAdvertisingPeer error: NSError!) {
        
    }
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser!, didReceiveInvitationFromPeer peerID: MCPeerID!, withContext context: NSData!, invitationHandler: ((Bool, MCSession!) -> Void)!) {
        
    }
}