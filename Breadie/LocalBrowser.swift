//
//  LocalClient.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/12/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import MultipeerConnectivity

/*class LocalBrowser : NSObject, MCSessionDelegate, MCNearbyServiceBrowserDelegate, UITableViewDataSource {
    var availableServers : [Int]
    var session : MCSession
    var peerID : MCPeerID
    var browser : MCBrowserViewController
    
    init(name : String) {
        peerID = MCPeerID(displayName: name)
        session = MCSession(peer: peerID)
        availableServers = []
        browser = MCBrowserViewController(serviceType: "JoinGame", session: session)
        
        super.init()
        session.delegate = self
    }
    
    func session(session: MCSession!, peer peerID: MCPeerID!, didChangeState state: MCSessionState) {
        println("Changed state")
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!, fromPeer peerID: MCPeerID!) {
        
    }
    
    func session(session: MCSession!, didStartReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!) {
        
    }
    
    func session(session: MCSession!, didFinishReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, atURL localURL: NSURL!, withError error: NSError!) {
        
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!, withName streamName: String!, fromPeer peerID: MCPeerID!) {
        
    }
    
    func browser(browser: MCNearbyServiceBrowser!, foundPeer peerID: MCPeerID!, withDiscoveryInfo info: [NSObject : AnyObject]!) {
        
    }
    
    func browser(browser: MCNearbyServiceBrowser!, lostPeer peerID: MCPeerID!) {
        
    }
    
    func browser(browser: MCNearbyServiceBrowser!, didNotStartBrowsingForPeers error: NSError!) {
        
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        return nil
    }
}*/