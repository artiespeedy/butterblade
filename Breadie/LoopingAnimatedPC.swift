//
//  LoopingAnimatedPC.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/16/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class LoopingAnimatedPixelCollider {
    var colliders : [PixelCollider]
    var textures : [SKTexture]
    var timer : NSTimeInterval
    var delay : NSTimeInterval
    var loopFrame : Int = 0
    
    init(imageName : String, length : Int, delay : NSTimeInterval) {
        self.delay = delay
        timer = 0
        colliders = []
        textures = []
        textures = loadSprites(imageName, amount: length)
        colliders = loadColliders(imageName, amount: length)
    }
    
    init(imageName : String, length : Int, delay : NSTimeInterval, startTime : NSTimeInterval) {
        self.delay = delay
        timer = startTime
        colliders = []
        textures = []
        textures = loadSprites(imageName, amount: length)
        colliders = loadColliders(imageName, amount: length)
    }
    
    var timeLength : NSTimeInterval {
        get {
            return delay * NSTimeInterval(textures.count)
        }
    }
    
    var currentFrame : Int {
        get {
            if timer < timeLength {
                return Int(floor(Double(timer/delay)))
            } else {
                timer = (timer - timeLength) + NSTimeInterval(loopFrame) * delay
                return Int(floor(Double(timer/delay)))
            }
        }
        set (newFrame) {
            timer = NSTimeInterval(newFrame) * delay
        }
    }
    
    var currentTexture : SKTexture {
        get {
            return textures[currentFrame]
        }
    }
    
    var currentCollider : PixelCollider {
        get {
            return colliders[currentFrame]
        }
    }
    
    func loadSprites(name : String, amount : Int) -> [SKTexture] {
        var s : [SKTexture] = []
        for i in 0..<amount {
            var texture = SKTexture(textureWithoutAA: "\(name)\(i)")
            s.append(texture)
        }
        return s
    }
    
    func loadColliders(name : String, amount : Int) -> [PixelCollider] {
        var output : [PixelCollider] = []
        for i in 0..<amount {
            var pc = PixelCollider(imageName: "\(name)\(i)")
            output.append(pc)
        }
        return output
    }
}