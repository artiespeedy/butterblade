//
//  LoopingAnimation.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/15/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class LoopingAnimation: Animation {
    var loopFrame = 0
    override var currentFrame : Int {
        get {
            if timer < timeLength {
                return Int(floor(Double(timer/delay)))
            } else {
                timer = (timer - timeLength) + NSTimeInterval(loopFrame) * delay
                return Int(floor(Double(timer/delay)))
            }
        }
        set (newFrame) {
            timer = NSTimeInterval(newFrame) * delay
        }
    }
}