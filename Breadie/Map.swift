//
//  NewMap.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/14/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class Map : SKSpriteNode {
    
    var tiles : [[Tile]]
    
    var tileSizeMultiplier : Int = 1 {
        didSet {
            for x in tiles {
                for y in x {
                    y.size = CGSize(width: y.textureSize, height: y.textureSize)
                    y.size *= CGFloat(tileSizeMultiplier)
                }
            }
            refresh()
        }
    }
    
    var tileSize : Int {
        get {
            return tiles[0][0].textureSize * tileSizeMultiplier
        }
    }
    
    subscript(x : Int, y : Int) -> Tile {
        get {
            return tiles[x][y]
        }
        
        set (newValue) {
            tiles[x][y] = newValue
            refresh()
        }
    }
    
    var tileHeight : Int {
        get {
            return tiles[0].count
        }
    }
    
    var tileWidth : Int {
        get {
            return tiles.count
        }
    }
    
    var rowsWithSlopes : [Int] = [-1]
    var slopeIndexes : [Int : Bool] = [:]
    var columnsWithSlopes : [Int] = [-1]
    
    override init() {
        let tiles = [[Tile(id: 0, isSolid: false, isClimbable: false)]]
        let backgroundTiles = [[Tile(id: 0, isSolid: false, isClimbable: false)]]
        self.tiles = tiles
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
    }
    
    init(tiles : [[Tile]]) {
        self.tiles = tiles
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
        calculateSlopes()
        reload()
        refresh()
    }
    
    init(content : [[String]]) {
        tiles = []
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        loadTilesFromContent(content)
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
        calculateSlopes()
        reload()
        refresh()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    func getTilePointForCoords(point : CGPoint) -> CGPoint? {
        if self.frame.contains(point) {
            var newP = point - self.frame.origin
            
            var x = Int(newP.x / CGFloat(tileSize))
            var y = Int(newP.y / CGFloat(tileSize))
            
            var toReturn = CGPoint(x: x, y: y)
            
            return toReturn
        } else {
            return nil
        }
    }
    
    func getTileForCoords(point : CGPoint) -> Tile {
        var p = getTilePointForCoords(point)
        return tiles[Int(p!.x)][Int(p!.y)]
    }
    
    func getCoordsInFrameForTilePoint(point : CGPoint) -> CGPoint {
        return point * CGFloat(tileSize)
    }
    
    func loadTilesFromContent(content : [[String]]) {
        let contents = content.reverse()
        tiles = [[Tile]](count: contents[0].count, repeatedValue: [Tile](count: contents.count, repeatedValue: Tile(id: 0, isSolid: false, isClimbable: false)))
        for x in 0..<contents[0].count {
            for y in 0..<contents.count {
                var splitBit = contents[y][x].componentsSeparatedByString(":")
                let id = splitBit[0].toInt()!
                var tile : Tile
                switch(splitBit[1]) {
                case "0":
                    tile = Tile(id: id, isSolid: false, isClimbable: false)
                case "0r":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: false, isClimbable: false, direction: Directions.Right)
                    } else {
                        tile = Tile(id: id, isSolid: false, isClimbable: false, direction: Directions.Right)
                    }
                    slopeIndexes[y] = true
                case "0l":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: false, isClimbable: false, direction: Directions.Left)
                    } else {
                        tile = Tile(id: id, isSolid: false, isClimbable: false, direction: Directions.Left)
                    }
                    slopeIndexes[y] = true
                case "1":
                    tile = Tile(id: id, isSolid: true, isClimbable: false)
                case "1r":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: true, isClimbable: false, direction: Directions.Right)
                    } else {
                        tile = Tile(id: id, isSolid: true, isClimbable: false, direction: Directions.Right)
                    }
                    slopeIndexes[y] = true
                case "1l":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: true, isClimbable: false, direction: Directions.Left)
                    } else {
                        tile = Tile(id: id, isSolid: true, isClimbable: false, direction: Directions.Left)
                    }
                    slopeIndexes[y] = true
                case "2":
                    tile = Tile(id: id, isSolid: false, isClimbable: true)
                case "2r":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: false, isClimbable: true, direction: Directions.Right)
                    } else {
                        tile = Tile(id: id, isSolid: false, isClimbable: true, direction: Directions.Right)
                    }
                    slopeIndexes[y] = true
                case "2l":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: false, isClimbable: true, direction: Directions.Left)
                    } else {
                        tile = Tile(id: id, isSolid: false, isClimbable: true, direction: Directions.Left)
                    }
                    slopeIndexes[y] = true
                case "3":
                    tile = Tile(id: id, isSolid: true, isClimbable: true)
                case "3r":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: true, isClimbable: true, direction: Directions.Right)
                    } else {
                        tile = Tile(id: id, isSolid: true, isClimbable: true, direction: Directions.Right)
                    }
                    slopeIndexes[y] = true
                case "3l":
                    if splitBit.count == 3 {
                        let backgroundID = splitBit[2].toInt()!
                        tile = Tile(id: id, backgroundID: backgroundID, isSolid: true, isClimbable: true, direction: Directions.Left)
                    } else {
                        tile = Tile(id: id, isSolid: true, isClimbable: true, direction: Directions.Left)
                    }
                    slopeIndexes[y] = true
                default:
                    tile = Tile(id: id, isSolid: false, isClimbable: false)
                }
                tiles[x][y] = tile
            }
        }
        
        rowsWithSlopes.removeAtIndex(0)
        columnsWithSlopes.removeAtIndex(0)
    }
    
    /*func loadTilesFromIntContent(content : [[String]]) {
        let contents = content.reverse()
        tiles = [[Tile]](count: contents[0].count, repeatedValue: [Tile](count: contents.count, repeatedValue: Tile(id: 0, isSolid: false, isClimbable: false)))
        for x in 0..<contents[0].count {
            for y in 0..<contents.count {
                let int = content[x][y].toInt()!
                let tileID = int.isolate(1)!
                let type = int.isolate(2)!
                let direction = int.isolate(3)
                let backID = int.isolate(4)
                if direction != nil {
                    if direction! == 0 {
                        switch(type) {
                        case 0:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: false, isClimbable: false, direction: Directions.Left)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        case 1:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: false, direction: Directions.Left)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        case 2:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: true, direction: Directions.Left)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        default:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: false, direction: Directions.Left)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        }
                    } else {
                        switch(type) {
                        case 0:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: false, isClimbable: false, direction: Directions.Right)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        case 1:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: false, direction: Directions.Right)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        case 2:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: true, direction: Directions.Right)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        default:
                            tiles[x][y] = Tile(id: tileID, backgroundID: backID!, isSolid: true, isClimbable: false, direction: Directions.Right)
                            if !contains(rowsWithSlopes, y) {
                                rowsWithSlopes.append(y)
                            }
                            if !contains(columnsWithSlopes, x) {
                                columnsWithSlopes.append(x)
                            }
                        }
                    }
                } else {
                    switch(type) {
                    case 0:
                        tiles[x][y] = Tile(id: tileID, isSolid: false, isClimbable: false)
                    case 1:
                        tiles[x][y] = Tile(id: tileID, isSolid: true, isClimbable: false)
                    case 2:
                        tiles[x][y] = Tile(id: tileID, isSolid: true, isClimbable: true)
                    default:
                        tiles[x][y] = Tile(id: tileID, isSolid: true, isClimbable: 0)
                    }
                }
            }
        }
    }*/
    
    func calculateSlopes() {
        for y in slopeIndexes.keys {
            var slopesInARow = 0
            for x in 0..<self.tileWidth {
                if (tiles[x][y].isSloped) {
                    slopesInARow++
                } else {
                    if slopesInARow > 0 {
                        var startIndexOfSlopes = x - slopesInARow
                        var endIndexOfSlopes = x
                        
                        var slopeOfLine : Double = 1/Double(slopesInARow)
                        
                        for tileIndex in startIndexOfSlopes..<endIndexOfSlopes {
                            for px in 0..<DEFAULT_TILE_SIZE {
                                if (tiles[tileIndex][y].direction == Directions.Right) {
                                    //X value to determine height
                                    var currentPixel = Double(px + ((endIndexOfSlopes - 1 - tileIndex) * DEFAULT_TILE_SIZE))
                                    tiles[tileIndex][y].pixelHeights[Int(15-px)] = abs(Int(Double(currentPixel) * -slopeOfLine))
                                } else {
                                    var currentPixel = Double(px + ((tileIndex - startIndexOfSlopes) * DEFAULT_TILE_SIZE))
                                    tiles[tileIndex][y].pixelHeights[Int(px)] = Int(Double(currentPixel) * slopeOfLine)
                                }
                            }
                            tiles[tileIndex][y].refreshTexture()
                        }
                        
                        slopesInARow = 0
                    }
                }
            }
        }
    }
    
    func reload() {
        self.removeAllChildren()
        
        for x in 0..<self.tileWidth {
            for y in 0..<self.tileHeight {
                addChild(tiles[Int(x)][Int(y)])
            }
        }
    }
    
    func refresh() {
        for x in 0..<tileWidth {
            for y in 0..<tileHeight {
                tiles[x][y].position = CGPoint(x: x * tileSize, y: y * tileSize)
            }
        }
    }
}
