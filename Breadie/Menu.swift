//
//  Menu.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/20/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

enum ScreenPositions : Int {
    case TopLeft = 1
    case TopMiddle = 2
    case TopRight = 3
    case MiddleLeft = 4
    case MiddleMiddle = 5
    case MiddleRight = 6
    case BottomLeft = 7
    case BottomMiddle = 8
    case BottomRight = 9
    
    var position : CGPoint {
        get {
            switch self.toRaw() {
            case 1:
                return CGPoint(x: 0, y: sceneFrame.height)
            case 2:
                return CGPoint(x: sceneFrame.width / 2, y: sceneFrame.height)
            case 3:
                return CGPoint(x: sceneFrame.width, y: sceneFrame.height)
            case 4:
                return CGPoint(x: 0, y: sceneFrame.height / 2)
            case 5:
                return CGPoint(x: sceneFrame.width / 2, y: sceneFrame.height / 2)
            case 6:
                return CGPoint(x: sceneFrame.width, y: sceneFrame.height / 2)
            case 7:
                return CGPoint(x: 0, y: 0)
            case 8:
                return CGPoint(x: sceneFrame.width / 2, y: 0)
            case 9:
                return CGPoint(x: sceneFrame.width, y: 0)
            default:
                return CGPointZero
            }
        }
    }
    
    var positionInScene : CGPoint {
        get {
            return self.position + CGPoint(x: 0, y: sceneFrame.origin.y)
        }
    }
}

class Menu : SKNode {
    var background : SKSpriteNode?
    
    override init() {
        
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("Menu does not support NSCoder")
    }
}