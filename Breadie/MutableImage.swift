//
//  MutableImage.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/1/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

struct Pixel {
    let r : Byte, g : Byte, b : Byte, a : Byte
    init(r : Byte, g : Byte, b : Byte, a : Byte) {
        self.r = r
        self.g = g
        self.b = b
        self.a = a
    }
    
    init(r : Int, g : Int, b : Int, a : Int) {
        self.r = Byte(r)
        self.g = Byte(g)
        self.b = Byte(b)
        self.a = Byte(a)
    }
    
    init(color : UInt32) {
        r = Byte((color & 0xFF000000) >> 24)
        g = Byte((color & 0x00FF0000) >> 16)
        b = Byte((color & 0x0000FF00) >> 8)
        a = Byte((color & 0x000000FF) >> 0)
    }
    
    func toUInt32() -> UInt32 {
        let red = UInt32(r) << 24
        let green = UInt32(g) << 16
        let blue = UInt32(b) << 8
        let alpha = UInt32(a) << 0
        let num = red + green + blue + alpha
        return num
    }
}

//Fully optimized and fast
struct MutableImage {
    var pixels : Array2D<Pixel>
    let width : Int
    let height : Int
    
    subscript(x : Int) -> [Pixel] {
        get {
            return pixels[x]
        }
        set (newValue) {
            pixels[x] = newValue
        }
    }
    
    init(name : String) {
        let image = UIImage(named: name)
        width = Int(image.size.width)
        height = Int(image.size.height)
        pixels = Array2D(width: width, height: height, fillerValue: Pixel(r: 0, g: 0, b: 0, a: 0))
        
        let imageData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage))
        var imageDataSize = CFDataGetBytePtr(imageData)
        var array : [Pixel] = []
        var i = 0
        while i < ((width * height) - 0) * 4 {
            let r = imageDataSize[i]
            let b = imageDataSize[i + 2]
            let g = imageDataSize[i + 1]
            let a = imageDataSize[i + 3]
            
            array.append(Pixel(r: r, g: g, b: b, a: a))
            i += 4
        }
        
        
        
        for i in 0..<array.count {
            let x = i % height
            let y = height - Int(i / height) - 1
            pixels[x][y] = array[i]
        }
    }
    
    var size : CGSize {
        get {
            return CGSize(width: width, height: height)
        }
    }
    
    var data : NSData {
        get {
            var data : [UInt8] = []
            var i : Int
            for x in 0..<width {
                for y in 0..<height {
                    var pixel = pixels[y][x]
                    data.append(UInt8(pixel.r))
                    data.append(UInt8(pixel.g))
                    data.append(UInt8(pixel.b))
                    data.append(UInt8(pixel.a))
                }
            }
            let nsData = NSData(bytes: &data, length: width * height * 4)
            return nsData
        }
    }
    
    var texture : SKTexture {
        get {
            var t = SKTexture(data: data, size: size)
            t.filteringMode = .Nearest
            return t
        }
    }
}

extension Int {
    mutating func cap(value : Int) {
        if self > value {
            self = value
        }
    }
}

//Slow as hell
func + (lhs : Pixel, rhs : Pixel) -> Pixel {
    if rhs.a == 255 {
        return rhs
    } else if rhs.a == 0 {
        return lhs
    } else {
        let ralphaMultiplier = (Float(rhs.a)/Float(255))
        var lalphaMultiplier = (Float(255) - Float(rhs.a))
        if lalphaMultiplier != 0 {
            lalphaMultiplier /= Float(255)
        }
        let r = Byte(Float(rhs.r) * ralphaMultiplier) + Byte(Float(lhs.r) * lalphaMultiplier)
        let g = Byte(Float(rhs.g) * ralphaMultiplier) + Byte(Float(lhs.g) * lalphaMultiplier)
        let b = Byte(Float(rhs.b) * ralphaMultiplier) + Byte(Float(lhs.b) * lalphaMultiplier)
        var a = Int(lhs.a) + Int(rhs.a)
        a.cap(255)
        return Pixel(r: r, g: g, b: b, a: Byte(a))
    }
}

func += (inout lhs : Pixel, rhs : Pixel) {
    lhs = lhs + rhs
}

//This is so slow its not even funny
func + (lhs : MutableImage, rhs : MutableImage) -> MutableImage {
    var toReturn = lhs
    for x in 0..<lhs.pixels.width {
        for y in 0..<rhs.pixels.height {
            toReturn[x][y] += rhs[x][y]
        }
    }
    return toReturn
}