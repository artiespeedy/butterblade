//
//  Map.swift
//  Breadie
//
//  Created by Alex Mai on 7/30/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//
/*
import Foundation
import SpriteKit

class Map : SKSpriteNode {
    
    var tiles : [[Tile]]
    var backgroundTiles : [[Tile]]
    
    var tileSize = 16
    
    var rowsWithSlopes : [Int] = [-1]
    var columnsWithSlopes : [Int] = [-1]
    
    override init() {
        let tiles = [[Tile(id: 0, isSolid: false, isHazardous: false)]]
        let backgroundTiles = [[Tile(id: 0, isSolid: false, isHazardous: false)]]
        self.tiles = tiles
        self.backgroundTiles = backgroundTiles
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
    }
    
    init(name : String, type : String) {
        tiles = []
        backgroundTiles = []
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        var temp = loadLevelFromFile(name, type: type)
        tiles = temp!.0
        backgroundTiles = temp!.1
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
        calculateSlopes()
        refresh()
    }
    
    init(tiles : [[Tile]], backgroundTiles : [[Tile]]) {
        self.tiles = tiles
        self.backgroundTiles = backgroundTiles
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
        calculateSlopes()
        refresh()
    }
    
    init(content : [[String]]) {
        tiles = []
        backgroundTiles = []
        super.init(texture: SKTexture(imageNamed: "Tile 0"), color: SKColor.blackColor(), size: CGSize(width: 0, height: 0))
        loadTilesFromContent(content)
        var pixelSize = CGSize(width: tiles.count * tileSize, height: tiles[0].count * tileSize)
        self.size = pixelSize
        self.anchorPoint = CGPointZero
        calculateSlopes()
        refresh()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    subscript(x : Int, y : Int) -> Tile {
        get {
            return tiles[x][y]
        }
        
        set (newValue) {
            tiles[x][y] = newValue
            refresh()
        }
    }
    
    var sizeMultiplier : Int {
        get {
            return tileSize / DEFAULT_TILE_SIZE
        }
        set (newSize) {
            tileSize = DEFAULT_TILE_SIZE * newSize
            self.size = CGSize(width: self.tileWidth * tileSize, height: self.tileHeight * tileSize)
            cTiles = nil
            refresh()
        }
    }
    
    var sprites : [[SKSpriteNode]] {
    get {
        var spriteArray = [[SKSpriteNode]](count: self.tileWidth, repeatedValue: [SKSpriteNode](count: self.tileHeight, repeatedValue: SKSpriteNode(imageWithoutAA: "Tile 0")))
        for x in 0..<self.tileWidth {
            for y in 0..<self.tileHeight {
                var tile = tiles[x][y]
                if tile.isSloped {
                    tile.tileSize = tileSize
                }
                var sprite = tile.getSprite()
                sprite.size = CGSize(width: tileSize, height: tileSize)
                sprite.anchorPoint = CGPointZero
                sprite.position = getCoordsInFrameForTilePoint(CGPoint(x: x, y: y))
                spriteArray[x][y] = sprite
            }
        }
        return spriteArray
    }
    }
    
    var backgroundSprites : [[SKSpriteNode]] {
    get {
        var spriteArray = [[SKSpriteNode]](count: self.tileWidth, repeatedValue: [SKSpriteNode](count: self.tileHeight, repeatedValue: SKSpriteNode(imageWithoutAA: "Tile 0")))
        for x in 0..<self.tileWidth {
            for y in 0..<self.tileHeight {
                var tile = backgroundTiles[x][y].getSprite()
                tile.size = CGSize(width: tileSize, height: tileSize)
                tile.anchorPoint = CGPointZero
                tile.position = getCoordsInFrameForTilePoint(CGPoint(x: x, y: y))
                spriteArray[x][y] = tile
            }
        }
        return spriteArray
    }
    }
    
    var cTiles : ([[CollisionTile]])?
    var collisionTiles : [[CollisionTile]] {
        get {
            if cTiles? == nil {
                var cTiles = [[CollisionTile]](count: tiles.count, repeatedValue: [CollisionTile](count: tiles[0].count, repeatedValue: CollisionTile(position: self.frame.origin, tileSize: tileSize, isSolid: true, isHazardous: false)))
                for x in 0..<tiles.count {
                    for y in 0..<tiles[0].count {
                        var position = CGPoint(x: self.frame.origin.x + CGFloat(x * tileSize), y: self.frame.origin.y + CGFloat(y * tileSize))
                        cTiles[x][y] = CollisionTile(fromTile: tiles[x][y], andPosition: position, andTileSize: tileSize)
                    }
                }
                return cTiles
            } else {
                return cTiles!
            }
        }
    }
    
    var tileHeight : Int {
        get {
            return tiles[0].count
        }
    }
    
    var tileWidth : Int {
        get {
            return tiles.count
        }
    }

    func getTilePointForCoords(point : CGPoint) -> CGPoint? {
        if self.frame.contains(point) {
            var newP = point - self.frame.origin
            
            var x = Int(newP.x / CGFloat(tileSize))
            var y = Int(newP.y / CGFloat(tileSize))
            
            var toReturn = CGPoint(x: x, y: y)
            
            return toReturn
        } else {
            return nil
        }
    }
    
    func getTileForCoords(point : CGPoint) -> Tile {
        var p = getTilePointForCoords(point)
        return tiles[Int(p!.x)][Int(p!.y)]
    }
    
    func getCoordsInFrameForTilePoint(point : CGPoint) -> CGPoint {
        return point * CGFloat(tileSize)
    }
    
    func getTilesForRect(rect : CGRect) -> ([[Tile]])? {
        if self.frame.intersects(rect) {
            var origin = getTilePointForCoords(rect.origin)
            var topRight = getTilePointForCoords(rect.origin + CGPoint(x: rect.size.width, y: rect.size.height))
            var maxRow = Int(topRight!.y)
            var minRow = Int(origin!.y)
            var maxColumn = Int(topRight!.x)
            var minColumn = Int(origin!.x)
            
            var toReturn = [[Tile]](count: maxRow - minRow, repeatedValue: [Tile](count: maxColumn - minColumn, repeatedValue: Tile(id: 0, isSolid: false, isHazardous: false)))
            
            for x in minRow..<maxRow {
                for y in minColumn..<maxColumn {
                    toReturn[x - minRow][y - minColumn] = tiles[x][y]
                }
            }
            
            return toReturn
        }
        return nil
    }
    
    func getCollisionTilesForRect(rect : CGRect) -> ([[CollisionTile]])? {
        var rectInFrame = CGRect(origin: rect.origin - self.frame.origin, size: rect.size)
        if self.frame.contains(rectInFrame) {
            var origin = getTilePointForCoords(rect.origin)
            var topRight = getTilePointForCoords(rect.origin + CGPoint(x: rect.size.width, y: rect.size.height))
            var maxRow = Int(topRight!.y)
            var minRow = Int(origin!.y)
            var maxColumn = Int(topRight!.x)
            var minColumn = Int(origin!.x)
            
            var toReturn = [[CollisionTile]](count: maxRow - minRow, repeatedValue: [CollisionTile](count: maxColumn - minColumn, repeatedValue: CollisionTile(position: self.frame.origin, tileSize: tileSize, isSolid: true, isHazardous: false)))
            
            var cTiles = collisionTiles
            
            for x in minRow..<maxRow {
                for y in minColumn..<maxColumn {
                    toReturn[x - minRow][y - minColumn] = cTiles[x][y]
                }
            }
            return toReturn
        }
        return nil
    }
    
    func getCollisionTileForPoint(point : CGPoint) -> CollisionTile {
        var pointInFrame = point - frame.origin
        if frame.contains(pointInFrame) {
            var x = Int(floor(pointInFrame.x / CGFloat(tileSize)))
            var y = Int(floor(pointInFrame.y / CGFloat(tileSize)))
            
            return collisionTiles[x][y]
        } else {
            return CollisionTile(position: point, tileSize: tileSize, isSolid: false, isHazardous: false)
        }
    }
    
    func loadTilesFromContent(content : [[String]]) {
        let contents = content.reverse()
        tiles = [[Tile]](count: contents[0].count, repeatedValue: [Tile](count: contents.count, repeatedValue: Tile(id: 0, isSolid: false, isHazardous: false)))
        backgroundTiles = [[Tile]](count: contents[0].count, repeatedValue: [Tile](count: contents.count, repeatedValue: Tile(id: 0, isSolid: false, isHazardous: false)))
        for x in 0..<contents[0].count {
            for y in 0..<contents.count {
                var splitBit = contents[y][x].componentsSeparatedByString(":")
                let id = splitBit[0].toInt()!
                var tile : Tile
                switch(splitBit[1]) {
                case "0":
                    tile = Tile(id: id, isSolid: false, isHazardous: false)
                case "0r":
                    tile = Tile(id: id, isSolid: false, isHazardous: false, direction: Directions.Right)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "0l":
                    tile = Tile(id: id, isSolid: false, isHazardous: false, direction: Directions.Left)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "1":
                    tile = Tile(id: id, isSolid: true, isHazardous: false)
                case "1r":
                    tile = Tile(id: id, isSolid: true, isHazardous: false, direction: Directions.Right)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "1l":
                    tile = Tile(id: id, isSolid: true, isHazardous: false, direction: Directions.Left)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "2":
                    tile = Tile(id: id, isSolid: false, isHazardous: true)
                case "2r":
                    tile = Tile(id: id, isSolid: false, isHazardous: true, direction: Directions.Right)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "2l":
                    tile = Tile(id: id, isSolid: false, isHazardous: true, direction: Directions.Left)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "3":
                    tile = Tile(id: id, isSolid: true, isHazardous: true)
                case "3r":
                    tile = Tile(id: id, isSolid: true, isHazardous: true, direction: Directions.Right)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                case "3l":
                    tile = Tile(id: id, isSolid: true, isHazardous: true, direction: Directions.Left)
                    if !contains(rowsWithSlopes, y) {
                        rowsWithSlopes.append(y)
                    }
                    if !contains(columnsWithSlopes, x) {
                        columnsWithSlopes.append(x)
                    }
                default:
                    tile = Tile(id: id, isSolid: false, isHazardous: false)
                }
                tiles[x][y] = tile
                
                if splitBit.count == 3 {
                    backgroundTiles[x][y] = Tile(id: splitBit[2].toInt()!, isSolid: false, isHazardous: false)
                }
            }
        }
        
        rowsWithSlopes.removeAtIndex(0)
        columnsWithSlopes.removeAtIndex(0)
    }
    
    func loadLevelFromFile(name : String, type : String) -> ([[Tile]], [[Tile]])? {
        var path = NSBundle.mainBundle().pathForResource(name, ofType: type)
        var possibleContent = String.stringWithContentsOfFile(path!, encoding: NSUTF8StringEncoding, error: nil)
        if let content = possibleContent {
            var splitContent = content.componentsSeparatedByString("; ")
            var rows = splitContent[0].componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            var rowsOfTiles : [[String]] = []
            for row in rows.reverse() {
                rowsOfTiles.append(row.componentsSeparatedByString("-"))
            }
            
            var tiles = [[Tile]](count: rowsOfTiles[0].count, repeatedValue: [Tile](count: rowsOfTiles.count, repeatedValue: Tile(id: 0, isSolid: false, isHazardous: false)))
            var backgroundTiles = [[Tile]](count: rowsOfTiles[0].count, repeatedValue: [Tile](count: rowsOfTiles.count, repeatedValue: Tile(id: 0, isSolid: false, isHazardous: false)))
            
            for x in 0..<rowsOfTiles[0].count {
                for y in 0..<rowsOfTiles.count {
                    var splitBit = rowsOfTiles[y][x].componentsSeparatedByString(":")
                    let id = splitBit[0].toInt()!
                    var tile : Tile
                    switch(splitBit[1]) {
                    case "0":
                        tile = Tile(id: id, isSolid: false, isHazardous: false)
                    case "0r":
                        tile = Tile(id: id, isSolid: false, isHazardous: false, direction: Directions.Right)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "0l":
                        tile = Tile(id: id, isSolid: false, isHazardous: false, direction: Directions.Left)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "1":
                        tile = Tile(id: id, isSolid: true, isHazardous: false)
                    case "1r":
                        tile = Tile(id: id, isSolid: true, isHazardous: false, direction: Directions.Right)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "1l":
                        tile = Tile(id: id, isSolid: true, isHazardous: false, direction: Directions.Left)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "2":
                        tile = Tile(id: id, isSolid: false, isHazardous: true)
                    case "2r":
                        tile = Tile(id: id, isSolid: false, isHazardous: true, direction: Directions.Right)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "2l":
                        tile = Tile(id: id, isSolid: false, isHazardous: true, direction: Directions.Left)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "3":
                        tile = Tile(id: id, isSolid: true, isHazardous: true)
                    case "3r":
                        tile = Tile(id: id, isSolid: true, isHazardous: true, direction: Directions.Right)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    case "3l":
                        tile = Tile(id: id, isSolid: true, isHazardous: true, direction: Directions.Left)
                        if !contains(rowsWithSlopes, y) {
                            rowsWithSlopes.append(y)
                        }
                        if !contains(columnsWithSlopes, x) {
                            columnsWithSlopes.append(x)
                        }
                    default:
                        tile = Tile(id: id, isSolid: false, isHazardous: false)
                    }
                    tiles[x][y] = tile
                    
                    if splitBit.count == 3 {
                        backgroundTiles[x][y] = Tile(id: splitBit[2].toInt()!, isSolid: false, isHazardous: false)
                    }
                }
            }
            
            rowsWithSlopes.removeAtIndex(0)
            columnsWithSlopes.removeAtIndex(0)
            
            return (tiles, backgroundTiles)
        }
        
        
        return nil
    }
    
    func calculateSlopes() {
        for y in rowsWithSlopes {
            var slopesInARow = 0
            for x in 0..<self.tileWidth {
                if (tiles[x][y].isSloped) {
                    slopesInARow++
                } else {
                    if slopesInARow > 0 {
                        
                        var startIndexOfSlopes = x - slopesInARow
                        var endIndexOfSlopes = x
                        
                        var slopeOfLine : Double = 1/Double(slopesInARow)
                        
                        for tileIndex in startIndexOfSlopes..<endIndexOfSlopes {
                            
                            tiles[tileIndex][y].slope = slopeOfLine
                            
                            for px in 0..<DEFAULT_TILE_SIZE {
                                if (tiles[tileIndex][y].direction == Directions.Right) {
                                    var currentPixel = Double(px + ((endIndexOfSlopes - 1 - tileIndex) * DEFAULT_TILE_SIZE))
                                    tiles[tileIndex][y].pixelHeights[Int(15-px)] = Int(Double(currentPixel) * -slopeOfLine)
                                } else {
                                    var currentPixel = Double(px + ((tileIndex - startIndexOfSlopes) * DEFAULT_TILE_SIZE))
                                    tiles[tileIndex][y].pixelHeights[Int(px)] = Int(Double(currentPixel) * slopeOfLine)
                                }
                            }
                        }
                        
                        slopesInARow = 0
                    }
                }
            }
        }
    }
    
    func refresh() {
        self.removeAllChildren()
        
        var backgroundTileSprites = self.backgroundSprites
        for x in 0..<self.tileWidth {
            for y in 0..<self.tileHeight {
                addChild(backgroundTileSprites[Int(x)][Int(y)])
            }
        }
        
        var tileSprites = self.sprites
        for x in 0..<self.tileWidth {
            for y in 0..<self.tileHeight {
                addChild(tileSprites[Int(x)][Int(y)])
            }
        }
    }
    
    func updateWithMainLocation(location : CGPoint) {
        
    }
}*/