//
//  Tile.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/12/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//
/*
import Foundation
import SpriteKit

class Tile {
    let id : Int
    let isSolid : Bool
    let isHazardous : Bool
    let isSloped : Bool
    var slope : Double = 0
    var direction : Directions
    
    var tileSize : Int = 1
    
    var pixelHeights : [Int] = []
    
    init(id : Int, isSolid : Bool, isHazardous : Bool) {
        self.id = id
        self.isSolid = isSolid
        self.isHazardous = isHazardous
        self.isSloped = false
        for x in 0..<DEFAULT_TILE_SIZE {
            pixelHeights.append(DEFAULT_TILE_SIZE)
        }
        direction = Directions.None
    }
    
    init(id : Int, isSolid : Bool, isHazardous : Bool, direction : Directions) {
        self.id = id
        self.isSolid = isSolid
        self.isHazardous = isHazardous
        self.isSloped = true
        for x in 0..<DEFAULT_TILE_SIZE {
            pixelHeights.append(DEFAULT_TILE_SIZE)
        }
        self.direction = direction
    }
    
    func getSprite() -> SKSpriteNode {
        if !isSloped {
            if id == 0 {
                var texture = SKTexture(imageNamed: "Tile \(id)")
                texture.filteringMode = SKTextureFilteringMode.Nearest
                return SKSpriteNode(texture: texture)
            } else {
                var texture = MutableImage(name: "Tile \(id)")
                return SKSpriteNode(texture: texture.texture)
            }
        } else {
            /*var image = UIImage(named: "Tile \(id)")
            var imgData = NSMutableData(data: UIImagePNGRepresentation(image))
            
            var range = NSRange(location: 0, length: 16)
            
            var data = imgData.subdataWithRange(range)
            
            var newImage = UIImage(data: imgData)
            var text = SKTexture(CGImage: newImage.CGImage)
            text.filteringMode = SKTextureFilteringMode.Nearest
            return SKSpriteNode(texture: text)*/
            return boundingBoxesAsSprites
        }
    }
    
    var slopedBoundingBoxes : [CGRect] {
        get {
            var boundingBoxes : [CGRect] = []
            var sizeMultiplier = CGFloat(tileSize / DEFAULT_TILE_SIZE)
            for x in 0..<DEFAULT_TILE_SIZE {
                boundingBoxes.append(CGRect(x: x, y: 0, width: 1, height: pixelHeights[x]))
            }
            return boundingBoxes
        }
    }
    
    var boundingBoxesAsSprites : SKSpriteNode {
        get {
            var boundingBoxes = self.slopedBoundingBoxes
            var node = SKSpriteNode()
            for i in boundingBoxes {
                var texture = SKSpriteNode(imageWithoutAA: "Tile \(id)").texture
                var adjustedRect = CGRect(x: i.origin.x / 16, y: (16-i.height)/16, width: 1/16, height: i.height/16)
                var partTexture = SKTexture(rect: adjustedRect, inTexture: texture)
                var sn = SKSpriteNode(texture: partTexture)
                var sizeMultiplier = CGFloat(tileSize / DEFAULT_TILE_SIZE)
                sn.anchorPoint = CGPointZero
                sn.position = i.origin * sizeMultiplier
                sn.size = CGSize(width: sizeMultiplier, height: i.height * sizeMultiplier)
                node.addChild(sn)
            }
            return node
        }
    }
    
    var boundingBoxesAsTextures : [SKTexture] {
        get {
            var boundingBoxes = self.slopedBoundingBoxes
            var array : [SKTexture] = []
            for i in boundingBoxes {
                var texture = SKSpriteNode(imageWithoutAA: "Tile \(id)").texture
                var adjustedRect = CGRect(x: i.origin.x / 16, y: (16-i.height)/16, width: 1/16, height: i.height/16)
                var partTexture = SKTexture(rect: adjustedRect, inTexture: texture)
                array.append(partTexture)
            }
            return array
        }
    }
    
    /*var ST : SKSpriteNode?
    
    var slopedTexture : SKSpriteNode {
    get {
    if ST == nil {
    
    var boundingBoxes = self.slopedBoundingBoxes
    
    UIGraphicsBeginImageContext(CGSize(width: DEFAULT_TILE_SIZE, height: DEFAULT_TILE_SIZE))
    
    var currentContext = UIGraphicsGetCurrentContext()
    
    for x in 0..<DEFAULT_TILE_SIZE {
    var originalTexture = UIImage(named: "Tile \(id)")
    var i = boundingBoxes[x]
    var shiftedUpRect = CGRect(x: CGFloat(x), y: CGFloat(DEFAULT_TILE_SIZE) - i.height, width: CGFloat(1), height: CGFloat(i.height))
    var shiftedTexture = originalTexture.imageWithAlignmentRectInsets(UIEdgeInsets(top: CGFloat(0), left: CGFloat(x), bottom: CGFloat(DEFAULT_TILE_SIZE) - i.height, right: CGFloat(DEFAULT_TILE_SIZE - x)))
    shiftedTexture.drawInRect(shiftedUpRect)
    }
    
    var image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    var texture = SKTexture(image: image)
    texture.filteringMode = SKTextureFilteringMode.Nearest
    
    return SKSpriteNode(texture: texture)
    } else {
    return ST!
    }
    }
    }*/
}*/
