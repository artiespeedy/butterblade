//
//  PixelCollider.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/15/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class PixelCollider {
    var pixelMask : [Bool]
    var pixelMaskWidth : CGFloat
    var pixelMaskHeight : CGFloat
    var pixelMaskSize : CGFloat
    var bitSize : UInt
    var alphaThreshold : UInt8 = 0
    
    var contents : [[Bool]]
    
    init(imageName : String) {
        var image = UIImage(named: imageName)
        pixelMaskHeight = image.size.height
        pixelMaskWidth = image.size.width
        pixelMaskSize = pixelMaskHeight * pixelMaskWidth
        bitSize = UInt(Int(pixelMaskSize) * sizeof(Bool))
        /*pixelMask = malloc(bitSize)
        memset(pixelMask, 0, bitSize)*/
        pixelMask = Array(count: Int(pixelMaskSize), repeatedValue: false)
        
        var imageData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage))
        let imagePs = CFDataGetBytePtr(imageData)
        let imagePixels = UnsafePointer<UInt32>(imagePs)
        var alphaValue : UInt32 = 0, x : UInt32 = 0
        //var y : UInt32 = UInt32(pixelMaskHeight) - 1
        var y : UInt32 = 0
        var alpha : UInt8 = 0
        
        for i in 0..<Int(pixelMaskSize - 1) {
            var index = y * UInt32(pixelMaskWidth) + x
            x++
            if x == UInt32(pixelMaskWidth) {
                x = 0
                //y--
                y++
            }
            
            alphaValue = imagePixels[i] & 0xff000000
            
            if alphaValue > 0 {
                alpha = UInt8(alphaValue >> 24)
                if alpha > alphaThreshold {
                    pixelMask[Int(index)] = true
                }
            }
        }
        contents = []
        contents = array2D
    }
    
    var p : ([[Bool]])?
    //Calculated instead of grab
    var array2D : [[Bool]] {
        get {
            if p == nil {
                var array = [[Bool]](count: Int(pixelMaskWidth), repeatedValue: [Bool](count: Int(pixelMaskHeight), repeatedValue: false))
                for x in 0..<Int(pixelMaskWidth - 1) {
                    for y in 0..<Int(pixelMaskHeight) {
                        var i = (x + 1) * y
                        array[x][y] = pixelMask[i]
                    }
                }
                p = array
                return array
            } else {
                return p!
            }
        }
    }
    
    func pixelsSurrounding(point : CGPoint) -> (Bool, Bool, Bool, Bool){
        //top, bottom, left, right
        var output = (false, false, false, false)
        
        var left = CGPoint(x: point.iX + 1, y: point.iY)
        var right = CGPoint(x: point.iX - 1, y: point.iY)
        var top = CGPoint(x: point.iX, y: point.iY + 1)
        var bottom = CGPoint(x: point.iX, y: point.iY - 1)
        
        //Check to make sure its within
        if contents.contains(top.iX) && contents.contains(top.iY) {
            output.0 = contents[top.iX][top.iY]
        } else {
            output.0 = false
        }
        
        if contents.contains(bottom.iX) && contents.contains(bottom.iY) {
            output.1 = contents[bottom.iX][bottom.iY]
        } else {
            output.1 = false
        }
        
        if contents.contains(left.iX) && contents.contains(left.iY) {
            output.2 = contents[left.iX][left.iY]
        } else {
            output.2 = false
        }
        
        if contents.contains(right.iX) && contents.contains(right.iY) {
            output.3 = contents[right.iX][right.iY]
        } else {
            output.3 = false
        }
        
        return output
    }
}