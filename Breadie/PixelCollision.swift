//
//  PixelCollision.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/17/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

struct PixelCollision {
    var pixelSet1 : [[Bool]]
    var pixelSet2 : [[Bool]]
    
    func combineSets(pos1 : CGPoint, pos2 : CGPoint) -> ([[Bool]], Bool) {
        
        var didCollide : Bool = false
        
        var width : Int
        var width1 = pos1.iX + pixelSet1.count
        var width2 = pos2.iX + pixelSet2.count
        
        (width1 > width2) ? (width = width1) : (width = width2)
        
        var height : Int
        var height1 = pos1.iY + pixelSet1[0].count
        var height2 = pos2.iY + pixelSet2[0].count
        
        (height1 > height2) ? (height = height1) : (height = height2)
        
        var startX : Int
        (pos1.iX > pos2.iX) ? (startX = pos2.iX) : (startX = pos1.iX)
        
        var startY : Int
        (pos1.iY > pos2.iY) ? (startY = pos2.iY) : (startY = pos1.iY)
        
        var combineSet = [[Bool]](count: width - startX, repeatedValue: [Bool](count: height - startY, repeatedValue: false))
        
        for x in startX..<width {
            for y in startY..<height {
                var bool1 : Bool
                var bool2 : Bool
                
                if pixelSet1.contains(x) && pixelSet1[0].contains(y) {
                    bool1 = pixelSet1[x][y]
                } else {
                    bool1 = false
                }
                
                if pixelSet2.contains(x) && pixelSet2[0].contains(y) {
                    bool2 = pixelSet2[x][y]
                } else {
                    bool2 = false
                }
                
                if bool1 && bool2 {
                    combineSet[x][y] = true
                    didCollide = true
                }
            }
        }
        
        return (combineSet, didCollide)
    }
}