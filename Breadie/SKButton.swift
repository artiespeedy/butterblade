//
//  SKButton.swift
//
//
//  Created by Alex Mai on 8/2/14.
//  Copyright (c) 2014 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKButton: SKSpriteNode {
    
    let pressedTexture : SKTexture
    let unpressedTexture : SKTexture
    let functionToCall : () -> ()
    var doesLoop = true
    
    var isPressed : Bool
    
    init(pressedTexture : SKTexture, unpressedTexture : SKTexture, functionToCall : () -> ())  {
        self.pressedTexture = pressedTexture
        self.unpressedTexture = unpressedTexture
        self.functionToCall = functionToCall
        isPressed = false
        super.init(texture: unpressedTexture, color: UIColor.whiteColor(), size: unpressedTexture.size())
        anchorPoint = CGPointZero
        userInteractionEnabled = true
        let bugFix = SKSpriteNode(texture: nil, color: nil, size: self.size)
        bugFix.anchorPoint = CGPointZero
        bugFix.position = CGPointZero
        addChild(bugFix)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoder not supported in SKButton")
    }
    
    func press() {
        isPressed = true
        functionToCall()
        self.texture = pressedTexture
    }
    
    func unpress() {
        isPressed = false
        self.texture = unpressedTexture
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        press()
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        var pressedThisEvent = false
        for i in touches.allObjects {
            let location = i.locationInNode(self.parent)
            if frame.contains(location) {
                if doesLoop {
                    press()
                }
                pressedThisEvent = true
            }
        }
        if !pressedThisEvent {
            unpress()
        }
        if !doesLoop {
            unpress()
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        unpress()
    }
    
    func update() {
        if doesLoop {
            if isPressed {
                functionToCall()
            }
        }
    }
}