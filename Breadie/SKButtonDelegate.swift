//
//  SKButtonDelegate.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/3/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKButtonDelegate: SKNode {
    var buttons : [SKButton]
    
    override init() {
        self.buttons = []
        super.init()
    }
    
    init(buttons : [SKButton]) {
        self.buttons = buttons
        super.init()
        refresh()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    
    func unpressAll() {
        for i in buttons {
            i.unpress()
        }
    }
    
    func addButton(button : SKButton) {
        buttons.append(button)
        addChild(button)
    }
    
    func updateAll(delta : CGFloat) {
        if buttons.count > 0 {
            for i in buttons {
                i.update()
            }
        }
    }
    
    func refresh() {
        removeAllChildren()
        for i in buttons {
            addChild(i)
        }
    }
}