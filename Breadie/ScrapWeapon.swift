
//
//  ScrapWeapon.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/17/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

//import Foundation

/*class Weapon : SKSpriteNode {
var weaponEdge : CGPath
var weaponShape : SKShapeNode
var particleNode : SKEmitterNode
var collisionRect : CGRect

init(name : String, type : String) {
//Temporary initializer to get past the super.init requirements - it needs to load from file
weaponEdge = CGPathCreateMutable()
weaponShape = SKShapeNode(path: weaponEdge)
particleNode = SKEmitterNode()
collisionRect = CGRect(x: 0, y: 0, width: 5, height: 5)
super.init(texture: SKTexture(imageNamed: "Tile 0"), color: UIColor.whiteColor(), size: CGSizeZero)
self.texture = loadWeapon(name, type: type)
}

required init(coder aDecoder: NSCoder!) {
fatalError("Weapon.swift does not support NSCoder")
}

func loadWeapon(name : String, type : String) -> SKTexture {
var path = NSBundle.mainBundle().pathForResource(name, ofType: type)
var potentialContents = String.stringWithContentsOfFile(path, encoding: NSUTF8StringEncoding, error: nil)
if let contents = potentialContents {
//Initialize variables to return
var shapePath : CGMutablePathRef
var shape : SKShapeNode
var particleNode : SKEmitterNode
var collisionRect : CGRect

var stringArray = contents.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())

//Load the weapon sprite
var texture = SKTexture(imageNamed: stringArray[0])
texture.filteringMode = SKTextureFilteringMode.Nearest

//Create the path
shapePath = CGPathCreateMutable()

//Load the particle system
particleNode = SKEmitterNode(fileNamed: stringArray[1])

//Go through all of the points detailed in the document
var transform = UnsafePointer<CGAffineTransform>.null()
for i in PATH_DATA_START_POINT..<stringArray.count {
var xy = stringArray[i].componentsSeparatedByString(", ")
if xy.count > 1 {
var x = xy[0].toInt()!
var y = xy[1].toInt()!
var CX : Int
var CY : Int
var radius : Int

if xy.count > 3 {
CY = xy[2].toInt()!
CX = xy[3].toInt()!
radius = xy[4].toInt()!
CGPathAddArcToPoint(shapePath, nil, CGFloat(x), CGFloat(y), CGFloat(CX), CGFloat(CY), CGFloat(radius))
} else {
CGPathAddLineToPoint(shapePath, nil, CGFloat(x), CGFloat(y))
}
} else {
fatalError("Missing component in txt file describing path")
}
}

shape = SKShapeNode(path: shapePath)

collisionRect = shape.frame

self.weaponEdge = shapePath
self.weaponShape = shape
self.particleNode = particleNode
self.particleNode.targetNode = self.weaponShape
self.collisionRect = weaponShape.frame
return texture
}
fatalError("Could not retrieve weapon file contents")
}
}*/