//
//  SlideOutMenu.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/26/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//
/*
import Foundation
import SpriteKit

class SlideOutMenu : SKSpriteNode {
    let slideOutDirection : Directions
    let buttonStackDirection : Directions
    var slideOut : SKAction
    var slideIn : SKAction
    let buttonSpacing : CGFloat
    
    init(slideOutDirection : Directions, buttonStackDirection : Directions, buttonSpacing : CGFloat, color : UIColor) {
        self.slideOutDirection = slideOutDirection
        self.buttonStackDirection = buttonStackDirection
        self.buttonSpacing = buttonSpacing
        slideOut = SKAction.moveToX(0, duration: 0)
        slideIn = SKAction.moveToX(0, duration: 0)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addButton(button : SKButton) {
        var buttonPos : CGPoint
        switch(buttonStackDirection) {
        case Directions.Right:
            buttonPos = frame.bottomRight + CGPoint(x: 10, y: 0)
            slideOut = SKAction.moveToX(size.width, duration: 0.2)
        case Directions.Up:
            buttonPos = frame.bottomRight + CGPoint(x: 0, y: button.size.height + buttonSpacing)
        case Directions.Down:
            buttonPos = frame.bottomRight + CGPoint(x: 0, y: button.size.height + buttonSpacing)
        case Directions.Left:
            buttonPos = frame.bottomRight + CGPoint(x: 10, y: 0)
        default:
            println("Direction not supported")
            buttonPos = CGPointZero
        }
        
        button.position = buttonPos
    }
}*/