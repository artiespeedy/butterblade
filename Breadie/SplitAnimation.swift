//
//  SplitAnimation.swift
//  ButterBlade
//
//  Created by Alex Mai on 8/15/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SplitAnimation {
    var doesLoop = true
    
    var animation1 : LoopingAnimation
    var animation2 : LoopingAnimation
    var topHalf : SKSpriteNode
    var target : Entity
    
    init(name1 : String, name2: String, length1: Int, length2: Int, delay1: NSTimeInterval, delay2: NSTimeInterval, target: Entity) {
        animation1 = LoopingAnimation(spriteName: name1, amountOfSprites: length1, delay: delay1)
        animation2 = LoopingAnimation(spriteName: name2, amountOfSprites: length2, delay: delay2)
        self.target = target
        
        topHalf = SKSpriteNode(texture: animation1.currentTexture)
        
        topHalf.anchorPoint = CGPointZero
        
        topHalf.size *= CGFloat(target.sizeMultiplier)
        
        topHalf.position = CGPoint(x: 0, y: target.size.height - topHalf.size.height)
        
        topHalf.name = "TopHalf"
        
        self.target = target
        self.target.addChild(topHalf)
    }
    
    init(a1: LoopingAnimation, a2: LoopingAnimation, target: Entity) {
        animation1 = a1
        animation2 = a2
        
        topHalf = SKSpriteNode(texture: animation1.currentTexture)
        
        topHalf.anchorPoint = CGPointZero
        
        topHalf.size *= CGFloat(target.sizeMultiplier)
        
        topHalf.position = CGPoint(x: 0, y: target.size.height - topHalf.size.height)
        
        topHalf.name = "TopHalf"
        
        self.target = target
        self.target.addChild(topHalf)
    }
    
    func update(delta : NSTimeInterval) {
        animation1.timer += delta
        animation2.timer += delta
        topHalf.texture = animation1.currentTexture
        target.texture = animation2.currentTexture
    }
    
    func cleanUp() {
        target.childNodeWithName("TopHalf")?.removeFromParent()
    }
}