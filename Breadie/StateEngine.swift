//
//  StateEngine.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/21/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

class StateEngine<StateKeyType: Hashable> {
    var states : [StateKeyType: Animation]
    var currentState : StateKeyType
    var target : SKSpriteNode?
    
    var currentAnimation : Animation {
        get {
            return states[currentState]!
        }
    }
    
    init(stateKey : StateKeyType, firstState : Animation) {
        states = [stateKey : firstState]
        currentState = stateKey
    }
    
    func addState(stateKey : StateKeyType, state : Animation) {
        states[stateKey] = state
    }
    
    func stopAndRestoreTo(stateNumber : StateKeyType) {
        currentAnimation.reset()
        currentState = stateNumber
    }
    
    func stopAndRestoreTo(stateNumber : StateKeyType, startingFrame : Int) {
        currentAnimation.reset()
        currentState = stateNumber
        currentAnimation.currentFrame = startingFrame
    }
    
    func update(delta : NSTimeInterval) {
        currentAnimation.update(delta)
        if let i = currentAnimation as? LoopingAnimation  {
            
        } else {
            if currentAnimation.timer > currentAnimation.timeLength {
                stopAndRestoreTo(states.keys.first!)
            }
        }
        if target != nil {
            target!.texture = currentAnimation.currentTexture
        }
    }
}