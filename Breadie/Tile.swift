//
//  NewTile.swift
//  ButterBlade
//
//  Created by Alex Mai on 9/1/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let DEFAULT_TILE_SIZE = 16

class TextureDictionary {
    var dictionary : [String: SKTexture] = [:]
    var secondary : [String: MutableImage] = [:]
    
    init() {
        
    }
    
    func getTexture(name : String) -> SKTexture {
        if dictionary[name] != nil {
            return dictionary[name]!
        } else {
            dictionary[name] = SKTexture(textureWithoutAA: name)
            return dictionary[name]!
        }
    }
    
    func getMutableTexture(name : String) -> MutableImage {
        if secondary[name] != nil {
            return secondary[name]!
        } else {
            secondary[name] = MutableImage(name: name)
            return secondary[name]!
        }
    }
}

var tileTextures = TextureDictionary()

class Tile : SKSpriteNode {
    let id : Int
    let bId : Int
    let isSolid : Bool
    let isClimbable : Bool
    let isSloped : Bool
    let direction : Directions
    let textureSize : Int
    var mutableTexture : MutableImage?
    var mutableBackgroundTexture : MutableImage?
    
    var tileSize : Int {
        get {
            return Int(self.size.height)
        }
        
        set (s) {
            self.size = CGSize(width: s, height: s)
        }
    }
    
    var tileSizeMultiplier : Int {
        get {
            return Int(tileSize / textureSize)
        }
        set (s) {
            self.size = self.size * CGFloat(s)
        }
    }
    
    var CGTileSizeMultiplier : CGFloat {
        get {
            return CGFloat(tileSize / textureSize)
        }
    }
    
    var pixelHeights : [Int] = []
    
    init(id : Int, isSolid : Bool, isClimbable : Bool) {
        self.id = id
        self.bId = 0
        self.isSolid = isSolid
        self.isClimbable = isClimbable
        self.isSloped = false
        for x in 0..<DEFAULT_TILE_SIZE {
            pixelHeights.append(DEFAULT_TILE_SIZE)
        }
        direction = Directions.None
        var t = tileTextures.getTexture("Tile \(id)")
        textureSize = Int(t.size().width)
        super.init(texture: t, color: UIColor.whiteColor(), size: t.size())
        anchorPoint = CGPointZero
    }
    
    init(id : Int, isSolid : Bool, isClimbable : Bool, direction : Directions) {
        self.id = id
        self.bId = 0
        self.isSolid = isSolid
        self.isClimbable = isClimbable
        self.isSloped = true
        for x in 0..<DEFAULT_TILE_SIZE {
            pixelHeights.append(DEFAULT_TILE_SIZE)
        }
        self.direction = direction
        mutableTexture = tileTextures.getMutableTexture("Tile \(id)")
        mutableBackgroundTexture = tileTextures.getMutableTexture("Tile \(bId)")
        
        let t = mutableTexture!.texture
        textureSize = Int(t.size().width)
        super.init(texture: t, color: UIColor.whiteColor(), size: t.size())
        anchorPoint = CGPointZero
    }
    
    init(id : Int, backgroundID : Int, isSolid : Bool, isClimbable : Bool, direction : Directions) {
        self.id = id
        self.bId = backgroundID
        self.isSolid = isSolid
        self.isClimbable = isClimbable
        self.isSloped = true
        for x in 0..<DEFAULT_TILE_SIZE {
            pixelHeights.append(DEFAULT_TILE_SIZE)
        }
        self.direction = direction
        mutableTexture = tileTextures.getMutableTexture("Tile \(id)")
        mutableBackgroundTexture = tileTextures.getMutableTexture("Tile \(bId)")
        
        let t = mutableTexture!.texture
        textureSize = Int(t.size().width)
        super.init(texture: t, color: UIColor.whiteColor(), size: t.size())
        anchorPoint = CGPointZero
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var slopedPixelBoxes : [CGRect] {
        get {
            var boundingBoxes : [CGRect] = []
            for x in 0..<pixelHeights.count {
                boundingBoxes.append(CGRect(x: CGFloat(x), y: 0, width: 1, height: CGFloat(pixelHeights[x])))
            }
            return boundingBoxes
        }
    }
    
    var slopedBoundingBoxes : [CGRect] {
        get {
            var boundingBoxes : [CGRect] = []
            for x in 0..<pixelHeights.count {
                boundingBoxes.append(CGRect(x: (CGFloat(x) * CGTileSizeMultiplier) + self.position.x, y: self.position.y, width: CGTileSizeMultiplier, height: CGFloat(pixelHeights[x]) * CGTileSizeMultiplier))
            }
            return boundingBoxes
        }
    }
    
    var boundingBoxesAsSprites : SKSpriteNode {
        get {
            var boundingBoxes = self.slopedPixelBoxes
            var node = SKSpriteNode()
            for i in boundingBoxes {
                var texture = tileTextures.getTexture("Tile \(id)")
                var adjustedRect = CGRect(x: i.origin.x / 16, y: (16-i.height)/16, width: 1/16, height: i.height/16)
                var partTexture = SKTexture(rect: adjustedRect, inTexture: texture)
                var sn = SKSpriteNode(texture: partTexture)
                var sizeMultiplier = CGFloat(tileSize / DEFAULT_TILE_SIZE)
                sn.anchorPoint = CGPointZero
                sn.position = i.origin * sizeMultiplier
                sn.size = CGSize(width: sizeMultiplier, height: i.height * sizeMultiplier)
                node.addChild(sn)
            }
            return node
        }
    }
    
    var boundingBoxesAsTextures : [SKTexture] {
        get {
            var boundingBoxes = self.slopedPixelBoxes
            var array : [SKTexture] = []
            for i in boundingBoxes {
                var texture = SKSpriteNode(imageWithoutAA: "Tile \(id)").texture
                var adjustedRect = CGRect(x: i.origin.x / 16, y: (16-i.height)/16, width: 1/16, height: i.height/16)
                var partTexture = SKTexture(rect: adjustedRect, inTexture: texture!)
                array.append(partTexture)
            }
            return array
        }
    }
    
    func refreshTexture() {
        if isSloped {
            for i in 0..<pixelHeights.count {
                let pushDistance = -(DEFAULT_TILE_SIZE - pixelHeights[i])
                mutableTexture!.pixels.shift(i, distance: pushDistance, fillerValue: Pixel(color: 0xFFFFFF00))
            }
            self.texture = (mutableBackgroundTexture! + mutableTexture!).texture
        }
    }
    
    func highestPointTouching(rect : CGRect) -> CGFloat {
        if self.isSloped {
            var highest : Int = 0
            for i in slopedBoundingBoxes {
                if rect.intersects(i) && Int(i.height) > highest {
                    highest = Int(i.height)
                }
            }
            return self.position.y + CGFloat(highest)
        } else {
            return self.frame.topLeft.y
        }
    }
    
    func slopeHeightAt(x : CGFloat) -> Int {
        var tileX = Int(floor(x / CGFloat(tileSize)))
        var pixelX = Int(floor(x) - CGFloat(tileX * tileSize))
        var slopeX = Int(CGFloat(pixelX) / CGTileSizeMultiplier)
        if isSloped {
            if slopeX < 16 && slopeX >= 0 {
                return Int(pixelHeights[slopeX])
            } else {
                return 16
            }
        } else if isSolid {
            return 16
        } else {
            return 0
        }
    }
    
    func slopeRectForX(x : CGFloat) -> CGRect {
        var tileX = Int(floor(x / CGFloat(tileSize)))
        var pixelX = Int(floor(x) - CGFloat(tileX * tileSize))
        var slopeX = Int(CGFloat(pixelX) / CGTileSizeMultiplier)
        if isSloped {
            if slopeX < 16 && slopeX >= 0 {
                return CGRect(x: Int(x), y: 0, width: 1, height: pixelHeights[slopeX])
            }
        }
        return CGRectZero
    }
    
    func heightAt(x : CGFloat) -> CGFloat {
        return (CGFloat(slopeHeightAt(x)) * CGTileSizeMultiplier) + position.y
    }
}