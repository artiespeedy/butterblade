//
//  Weapon.swift
//  ButterBlade
//
//  Created by Eric Mai on 8/9/14.
//  Copyright (c) 2014 Eric Mai. All rights reserved.
//

import Foundation
import SpriteKit

let DOES_MOVE_CANCEL = true
let defaultWeaponSpecs : [(Int, CGFloat, NSTimeInterval, NSTimeInterval)] = [
    (1, 3.5, 0.01, 0.01),
    (8, 3.5, 0.60, 0.4),
    (8, 3.5, 0.06, 0.2),
    (1, 3.5, 5.5, 5.5),
    (1, 3.5, 5.5, 5.5),
    (1, 3.5, 5.5, 5.5)
]

enum Move : Int {
    case UpperCut = 0
    case OverHead = 1
    case Stab = 2
    case SliceUp = 3
    case SliceDown = 4
    case None = 5
    
    func toStringRaw() -> String {
        switch(self.toRaw()) {
        case 0:
            return "UpperCut"
        case 1:
            return "OverHead"
        case 2:
            return "Stab"
        case 3:
            return "SliceUp"
        case 4:
            return "SliceDown"
        case 5:
            return "None"
        default:
            return "Nope"
        }
    }
}

class Weapon : SKSpriteNode {
    //Animation length, damage, forward speed, backward speed
    var stats : [(Int, CGFloat, NSTimeInterval, NSTimeInterval)]
    
    var isRetracting = false
    
    //var stab : AnimatedPixelCollider
    //var upperCut : AnimatedPixelCollider
    var overHead : AnimatedPixelCollider
    //var sliceUp : AnimatedPixelCollider
    //var sliceDown : AnimatedPixelCollider
    var idle : AnimatedPixelCollider
    
    /*var rStab : AnimatedPixelCollider
    var rUpperCut : AnimatedPixelCollider
    var rOverHead : AnimatedPixelCollider
    var rSliceUp : AnimatedPixelCollider
    var rSliceDown : AnimatedPixelCollider*/
    
    var cm : Move
    var previousMove : Move
    var currentMove : Move {
        get {
            return cm
        }
        set (newMove) {
            if newMove == currentMove {
                
            } else {
                if previousMove != cm {
                    previousMove = cm
                }
                cm = newMove
            }
        }
    }
    
    /*init(name : String, stats : [(Int, CGFloat, NSTimeInterval, NSTimeInterval)]) {
        self.stats = stats
        stab = AnimatedPixelCollider(imageName: "\(name)Stab", length: stats[0].0, delay: stats[0].2)
        rStab = stab.reversedVersion()
        rStab.delay = stats[0].3
        upperCut = AnimatedPixelCollider(imageName: "\(name)UpperCut", length: stats[1].0, delay: stats[1].2)
        rUpperCut = upperCut.reversedVersion()
        rUpperCut.delay = stats[1].3
        overHead = AnimatedPixelCollider(imageName: "\(name)OverHead", length: stats[2].0, delay: stats[2].2)
        rOverHead = overHead.reversedVersion()
        rOverHead.delay = stats[2].3
        sliceUp = AnimatedPixelCollider(imageName: "\(name)SliceUp", length: stats[3].0, delay: stats[3].2)
        rSliceUp = sliceUp.reversedVersion()
        rSliceUp.delay = stats[3].3
        sliceDown = AnimatedPixelCollider(imageName: "\(name)SliceDown", length: stats[4].0, delay: stats[4].2)
        rSliceDown = sliceDown.reversedVersion()
        rSliceDown.delay = stats[4].3
        idle = AnimatedPixelCollider(imageName: "\(name)Idle", length: stats[5].0, delay: stats[5].2)
        cm = Move.None
        previousMove = Move.None
        super.init(texture: currentCollider.currentTexture, color: UIColor.whiteColor(), size: currentCollider.currentTexture.size())
    }*/
    
    init(name : String) {
        stats = []
        var path = NSBundle.mainBundle().pathForResource(name, ofType: "txt")
        if path != nil {
            var potential = String.stringWithContentsOfFile(path!, encoding: NSUTF8StringEncoding, error: nil)
            if let content = potential {
                var lines = content.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
                for move in lines {
                    var info = move.componentsSeparatedByString(":")
                    var length = info[0].toInt()
                    var damage : CGFloat = CGFloat(info[1].toFloat())
                    var forwardTime : NSTimeInterval = info[2].toDouble()
                    var backwardTime : NSTimeInterval = info[3].toDouble()
                    var specs = (length!, damage, forwardTime, backwardTime)
                    stats.append(specs as (Int, CGFloat, NSTimeInterval, NSTimeInterval))
                }
            } else {
                stats = defaultWeaponSpecs
            }
        } else {
            stats = defaultWeaponSpecs
        }
        /*stab = AnimatedPixelCollider(imageName: "\(name)Stab", length: stats[0].0, delay: stats[0].2)
        rStab = stab.reversedVersion()
        rStab.delay = stats[0].3
        upperCut = AnimatedPixelCollider(imageName: "\(name)UpperCut", length: stats[1].0, delay: stats[1].2)
        rUpperCut = upperCut.reversedVersion()
        rUpperCut.delay = stats[1].3*/
        overHead = AnimatedPixelCollider(imageName: "\(name)OverHead", length: stats[2].0, delay: stats[2].2)
        //rOverHead = overHead.reversedVersion()
        //rOverHead.delay = stats[2].3
        /*sliceUp = AnimatedPixelCollider(imageName: "\(name)SliceUp", length: stats[3].0, delay: stats[3].2)
        rSliceUp = sliceUp.reversedVersion()
        rSliceUp.delay = stats[3].3
        sliceDown = AnimatedPixelCollider(imageName: "\(name)SliceDown", length: stats[4].0, delay: stats[4].2)
        rSliceDown = sliceDown.reversedVersion()
        rSliceDown.delay = stats[4].3*/
        idle = AnimatedPixelCollider(imageName: "\(name)Idle", length: stats[5].0, delay: stats[5].2)
        cm = Move.None
        previousMove = Move.None
        super.init(texture: currentCollider.currentTexture, color: UIColor.whiteColor(), size: currentCollider.currentTexture.size())
        anchorPoint = CGPointZero
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("Weapen does not support NSCoder!")
    }
    
    var wielder : Entity {
        get {
            return (parent as Entity)
        }
    }
    
    var positionInEntitySpace : CGPoint {
        get {
            return position + parent!.position
        }
    }
    
    var frameInEntitySpace : CGRect {
        get {
            return CGRect(origin: positionInEntitySpace, size: currentCollider.currentTexture.size())
        }
    }
    
    func colliderForMove(move : Move) -> AnimatedPixelCollider {
        /*if isRetracting {
            switch(move) {
            case Move.Stab:
                return rStab
            case Move.SliceUp:
                return rSliceUp
            case Move.SliceDown:
                return rSliceDown
            case Move.OverHead:
                return rOverHead
            case Move.UpperCut:
                return rUpperCut
            case Move.None:
                return idle
            default:
                return idle
            }
        } else {
            switch(move) {
            case Move.Stab:
                return stab
            case Move.SliceUp:
                return sliceUp
            case Move.SliceDown:
                return sliceDown
            case Move.OverHead:
                return overHead
            case Move.UpperCut:
                return upperCut
            case Move.None:
                return idle
            default:
                return idle
            }
        }*/
        if move == Move.None {
            return idle
        } else {
            return overHead
        }
    }
    
    var currentCollider : AnimatedPixelCollider {
        get {
            return colliderForMove(currentMove)
        }
    }
    
    func cancelCurrentMove() {
        currentMove = Move.None
    }
    
    func retract() {
        colliderForMove(currentMove).timer = 0
        
    }
    
    func collidesWith(weapon : Weapon) -> Bool {
        if weapon.frame.intersects(frame) {
            var thisWeapon = currentCollider
            var otherWeapon = weapon.currentCollider
            var collision = PixelCollision(pixelSet1: thisWeapon.currentCollider.array2D, pixelSet2: otherWeapon.currentCollider.array2D)
            return collision.combineSets(positionInEntitySpace, pos2: weapon.positionInEntitySpace).1
        } else {
            return false
        }
    }
    
    func collidersWith(rect : CGRect) -> Bool {
        if rect.intersects(CGRect(origin: position + parent!.position, size: currentCollider.currentTexture.size())) {
            var data = currentCollider.currentCollider.array2D
            for x in 0..<data.count {
                for y in 0..<data[0].count {
                    if rect.contains(CGPoint(x: x, y: y)) && data[x][y] {
                        return true
                    }
                }
            }
            return false
        } else {
            return false
        }
    }
    
    func update(delta : NSTimeInterval) {
        var cc = currentCollider
        cc.timer += delta
        if cc.timer > cc.timeLength {
            cc.timer = 0
            currentMove = Move.None
        }
        texture = cc.currentTexture
        var wielderDistanceToLeftEdge = (wielder.size.width / 2) - (5 * CGFloat(wielder.sizeMultiplier))
        var weaponDistanceToLeftEdge = (texture!.size().width / 2) - 5
        var x = (-weaponDistanceToLeftEdge * CGFloat(wielder.sizeMultiplier)) + wielderDistanceToLeftEdge
        var y : CGFloat = -CGFloat(wielder.sizeMultiplier)
        position = CGPoint(x: x, y: y)
    }
    
    func loadStats(name : String) -> [(Int, CGFloat, NSTimeInterval, NSTimeInterval)] {
        var path = NSBundle.mainBundle().pathForResource(name, ofType: "txt")
        var potential = String.stringWithContentsOfFile(path!, encoding: NSUTF8StringEncoding, error: nil)
        if let content = potential {
            var output : [(Int, CGFloat, NSTimeInterval, NSTimeInterval)] = []
            var lines = content.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            for move in lines {
                var info = move.componentsSeparatedByString(":")
                var length = info[0].toInt()
                var damage : CGFloat = CGFloat(info[1].toFloat())
                var forwardTime : NSTimeInterval = info[2].toDouble()
                var backwardTime : NSTimeInterval = info[3].toDouble()
                var specs = (length!, damage, forwardTime, backwardTime)
                output.append(specs as (Int, CGFloat, NSTimeInterval, NSTimeInterval))
            }
            
            return output
        } else {
            return defaultWeaponSpecs
        }
    }
}

